# Purpose of this document 

The goal of this document is to list missing standards and norms that may require some work in the next month or years. 
It’s based on our return on experience of GXFS-FR.

# Trusted List 

A Verifiable Credential is signed by a key. But it does not mean the Document is valid even if the key is eiDAS (trusted). A user can create a company and request an eiDAS certificate and sign a Verifiable Credential for a diploma without having this accreditation.
To improve trust, it is necessary to have a list that include keys but also the list of documents and geographic scope that a key can issue and over a date range. eg an auditor may issue an HDS Compliance in France between 2020 and 2023. 

W3C Does not provide any standard for this purpose. 
EBSI provide a list of trusted keys but only for entities and keys managed by EBSI. EBSI is using did:ebsi keys and Gaia-x did:web keys so the trusted registry of EBSI is not usable for legal documents. The registry cannot be updated for specific purpose (for example for Gaia-x to trust some documents issued by Federations...)

The Gaia-x solution is based on a Trust Registry with RegEx to have a solution but as temporary solution. RegEx is limited and complex to fix when a schema is updated. 

A component to manage this list should be provided by EU for regulated documents and allow companies to customize the list. 

# Signature and Verifyer

Allowed signatures for Verifiable Credentials is not defined in EU (SD-JWS ?...).

EU should define allowed keys and signature for all Verifiable Credentials to have a legal validity.
The topic should be defined by eiDAS but it's not the case for now.

Selective Disclosure is not regulated yet. The advantage of selective disclosure is to provide only required fields of a Verifiable Credential increasing privacy and keeping the ability for the verifyer to check the signature. It's a major improvement but requires signature like BBS+ that are not regulated yet. 

EU should provide a list of accepted signatures and provide a component implementing them as a reference to test signature. We’ve face to some integration issues due to several interpretations of specifications of signature methods. Provide a reference component may help to check solutions will be interoperable. 

EU should provide a component to verify signature of the Signature component and any other component to check issued Verifiable Credential are properly verified. 

# Consents

Consent management refers to the process by which an organization requests, obtains, and records the agreement of per to use their personal data. 
It typically involves informing about what data is being collected, how it will be used, and providing them with a clear choice to consent or opt out.
For now, in our understanding of that topic: 
* No standard to define consents in a structured format
* No standards to allow a provider to check consents have been provided. How to check a third party saying all consents have been provided is trusted? What’s included in that trust?

From a legal point of view this topic is covered but not yet with structured formats, all checks must be done manually. It's a hindrance to data exchange.

Consents should be defined in ODRL to have associated constrains (eg allow usage of health data by public health research in Europe only). 
That's a key point to automate and improve controls of data usage. 

A component may be provided by EU to store consents and compliant with EU regulations. This component provides APIs to check consents for a data usage and optionally to define ways to request missing consents for this usage. 

# Contracts

For now contracts are regulated for human readable contracts (pdf) including signatures allowed, how to timestamp, how to archive ... 
To automate contracts, a machine-readable format should be defined using ODRL but is not yet regulated.

EU should define mandatory fields to have a valid contract or terms and conditions in EU. 
for example, how to describe parties, what kind of identifiers are allowed for parties across Europe. This standardization should allow ODRL to be used with a legal value. 

For Gaia-x, both human and machine readable are sent to a Digital Signature Solution. The machine-readable format is displayed in a pdf format to be approved by an end user to have a legal value. This display is not user friendly but mandatory to have a legal value.

Digital Signature Solutions should be able to display and sign ODRL contracts directly. The ODRL contracts should have a legal value.

A component may be done to display ODRL contract and provide by EU. The goal is to have an ODRL contract as reference and to display it to a user and to sign this ODRL contract. 

# Legal audit trace component 

Audit traces are required by EU regulations for a lot of events. 
A standard component should be defined and provided for these traces with a compliance with existing european regulations.

This component may use new eiDAS regulation to provide a blockchain to increase security (auditability, inalterability…).
This component must be fast to manage huge volumes and be fault tolerant.

