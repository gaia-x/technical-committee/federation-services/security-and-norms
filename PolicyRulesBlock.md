**TEMP DOCUMENT**

# Purpose

The goal is to store any kind of rules set.
3 Rules Set MUST be provided:
| Rules Set | Output | Purpose |
| ------ | ------ | ------ |
| Compliance Service | Verifiable Credential | Check the service description is gaia-x compliant service description |
| Compliance Participant | Verifiable Credential | Check the description is gaia-x compliant participant description |
| Labelling | Verifiable Credential | Provide a Label to a Service Description  |

a Rule Set is a rego script. 

# References 

| Document | Link |  
|------|------------|
| Ontologies  | https://schemas.abc-federation.gxfs.fr/22.04/vocab/participant/index.html |

# APIs 

Rego scripts MUST be provided as [OPA bundles](https://www.openpolicyagent.org/docs/latest/management-bundles/).

These bundles MAY be provided using any web server like gitlab.

Rules MUST use "tags" in a namming like {ruleset}-{tag} (eg complianceParticipant-v2).

# Rule Set

## Prerequisites 

All rego policies rules MUST:
- accept a json in input
- provide a json output 

> How to manage versions of the script ? 

## Compliance Service Rules Set 

A script is provided to: 

| Rule id | Description |
| ------ | ------ |
| CSR_1 | check json-ld validity |
| CSR_2 | check gaia-x schemas gax-core and gax-service used and verified |
| CSR_3 | check signatures validity as defined by W3C  |
| CSR_4 | all URI provided MUST return HTTP CODE 2xx on a http call | 
| CSR_5 | provider participant MUST have a "complianceCredential" VC | 
| CSR_6 | All DID are resolvable using a did resolver | 
| CSR_7 | All claims need to be verified | 

A DEFINIR PLUS PRECISEMENT : quelle location prendre, le pays, adresse legale du participant ... 
| Service Offering  | TF 22.04   | gdpr attributes are mandatory when the service is provided in EEA or when the providedBy participant is located in EEA. | https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/service/#service-offering |
| Service Offering  | TF 22.04   | lgpd attributes are mandatory when the service is provided in Brazil or when the providedBy participant is located in Brazil. | https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/service/#service-offering |
| Service Offering  | TF 22.04   | pdpa attributes are mandatory when the service is provided in Singapore or when the providedBy participant is located in Singapore. | https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/service/#service-offering |

In case of success:
* Successfull http 200 OK request 
* content 
```
{    
  "@context": [
      "https://www.w3.org/2018/credentials/v1" ,
       {
         "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
       }
    ],
    "@id": "https://ruleschecker.abc-federation.gaia-x.community/vc/xxx",
    "@type": [
      "VerifiableCredential", "gax-compliance:ComplianceObject"
    ],
    "credentialSubject": {
      "vc": "did:web:ovhcloud.provider.gaia-x.community:participant:xxx/service-offering/vc/xxx/data.json",
      "@type" : "gax-compliance:ComplianceObject",
      "targetType" : "LocatedServiceOffering",
      "vcHash": "31296ef8727fc63e89213f3e5ab928bae7699b6dade0af5e443b8ad2623639ee8e6176696a1fa125ccfa7fa55465463b6424d3f54435d6a2beafd79a91425b0a",
      "validRules": [
        "CSR01_checkJsonld",
        "CSR02_checkSchemas",
        "CSR03_checkSignature",
        "CSR04_checkHttpCode",
        "CSR05_checkProviderVC",
        "CSR06_checkDid",
        "CSR07_checkClaims"
      ]
    },
    "evidence": {
      "documentPresent": "digital",
      "subjectPresent": "digital",
      "type": [],
      "verifier": ""
    },
    "expirationDate": "2022-10-26T15:29:32.872Z",
    "issuer": "did:web:abc-federation.gaia-x.community2",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2022-10-26T15:29:32.882456+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..taNLLHtr5IWgTgTBuJS1eS7uiZHs5KF_ogEjPVi7JXKaynKxrMKvg8_me7ecaWdRIgWKKfd4SY0caHta3bGlc4-ZoLFgAqKmCnom9zh9rFEOdPdhumDNOCyMAy6yjn37-4goaiTls2kz3NWGON0yH-g3kbd5mg6DmEV8JtAkA13QvBKBSjmwDdEwoAihqDxOsa-Dpqwe3N_DswBzvprcWzAARZVlOm8lkrMpBpqD9OvngaJxChGj1IQFnXT0LWwai-ENsCzkl9bsxAkFbWazJpRulBp_-Fz5YBjnNbXG1RqwrBU3ws5sxGSYNrwO7TcYHKqOEKfB2yuo3GOGI1GbmA"
}
```

In case of error:
* content 
```
{
    "result": {
        "lstNonValid": [
            "CSR04_checkHttpCode"
        ],
        "valid": "false"
    }
}
```

## Compliance Participant Rules Set

Schema of a [legalParticipant](https://sd-schemas.gaiax.ovh/wip/json-validation/legal-person.json)

| Rule id | Description |
| ------ | ------ |
| CPR_1 | check json-ld validity |
| CPR_2 | check gaia-x schemas : schema http://w3id.org/gaia-x/participant# gax-core and gax-participant used and verified |
| CPR_3 | check signatures validity as defined by W3C VC & VP | 
| CPR_4 | If legalAddress.country is located in European Economic Area, Iceland, Lichtenstein and Norway then registrationNumber must be a valid ISO 6523 EUID as specified in the section 8 of the Commission Implementing Regulation (EU) 2015/884. https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/ https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32020R2244&from=en#d1e1428-3-1 https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:European_Economic_Area_(EEA)  This number can be found via the [EU Business registers portal](https://e-justice.europa.eu/content_find_a_company-489-en.do)  |
| CPR_5 | If legalAddress.country is located in United States of America, than a valid legalAddress.state using the two-letter state abbreviations is mandatory | 
| CPR_6 | leiCode.headquarter.country shall equal headquarterAddress.country. | 
| CPR_7 | leiCode.legal.country shall equal legalAddress.country. |
| CPR_8 | if vatID is provided and headquarterAddress.countryCode belongs to the European member states or North Ireland, the number will be checked against the VAT Information Exchange System (VIES) API | 
| CPR_9 | All DID are resolvable using a did resolver | 


In case of success:
* Successfull http 200 OK request 
* content 
```
{    
  "@context": [
      "https://www.w3.org/2018/credentials/v1" ,
       {
         "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
       }
    ],
    "@id": "https://ruleschecker.abc-federation.gaia-x.community/vc/xxx",
    "@type": [
      "VerifiableCredential", "gax-compliance:ComplianceObject"
    ],
    "credentialSubject": {
      "vc": "did:web:ovhcloud.provider.gaia-x.community:participant:xxx/data.json",
      "@type" : "gax-compliance:ComplianceObject",
      "targetType" : "Participant",
      "vcHash": "31296ef8727fc63e89213f3e5ab928bae7699b6dade0af5e443b8ad2623639ee8e6176696a1fa125ccfa7fa55465463b6424d3f54435d6a2beafd79a91425b0a",
      "validRules": [
        "CPR01_checkJsonld",
        "CPR03_checkSignature",
        "CPR04_checkRegNum",
        "CPR05_checkUSState",
        "CPR06_checkLeiCodeInHQuarter",
        "CPR07_checkLeiCodeInLegAddress",
        "CPR09_checkDid"
      ]
    },
    "evidence": {
      "documentPresent": "digital",
      "subjectPresent": "digital",
      "type": [],
      "verifier": ""
    },
    "expirationDate": "2022-10-26T15:29:32.872Z",
    "issuer": "did:web:abc-federation.gaia-x.community2",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2022-10-26T15:29:32.882456+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..taNLLHtr5IWgTgTBuJS1eS7uiZHs5KF_ogEjPVi7JXKaynKxrMKvg8_me7ecaWdRIgWKKfd4SY0caHta3bGlc4-ZoLFgAqKmCnom9zh9rFEOdPdhumDNOCyMAy6yjn37-4goaiTls2kz3NWGON0yH-g3kbd5mg6DmEV8JtAkA13QvBKBSjmwDdEwoAihqDxOsa-Dpqwe3N_DswBzvprcWzAARZVlOm8lkrMpBpqD9OvngaJxChGj1IQFnXT0LWwai-ENsCzkl9bsxAkFbWazJpRulBp_-Fz5YBjnNbXG1RqwrBU3ws5sxGSYNrwO7TcYHKqOEKfB2yuo3GOGI1GbmA"
}
```

In case of error : 
* content 
```
{
    "result": {
        "lstNonValid": [
            "CPR09_checkDid"
        ],
        "valid": "false"
    }
}
```

TO DO check ou est le lien vers le VC LegalPerson , la signature du service et la signature du particippant 

## Labelling Rules Set 

| Rule id | Description | 
| ------ | ------ |
| LR_1 | check json-ld validity | 
| LR_2 | check gaia-x schemas : schema http://w3id.org/gaia-x/participant# gax-core and gax-participant used and verified |
| LR_3 | check signatures validity as defined by W3C VC & VP |
| LR_4 | check if a VC a valid "self assement" is provided or LR_50 |
| LR_5 | All DID are resolvable |
| LR_50 | check if a VC Criterion (FIND REF SCHEMA) is provided and signed by a CAB (Registred as CAB in the Trusted Registry)|
| LR_5 | ... |
| ... | ... |   

Each label is define by a set of criterion, to validate label n your service have to validate label n-1. 
List of criterion to validate labels : https://gitlab.com/gaia-x/policy-rules-committee/label-document/-/blob/master/docs/policy_rules_labeling_document.md

In case of success:
* Successfull http 200 OK request 
* content 
```
{ 
  "@context": [
    "https://www.w3.org/2018/credentials/v1" ,
    {
      "gax-labelling": "https://schemas.abc-federation.gaia-x.community/wip/vocab/labelling#",
    }
  ],
  "@id": "https://ruleschecker.abc-federation.gaia-x.community/vc/xxx",
  "@type": [
    "VerifiableCredential", "GrantedLabel"
  ],
  "credentialSubject": {
    "id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/granted-label/16ea3d9c5c9ef155dfee355366b44fc7119afdbeb84b809d54518b253d67310d/data.json",
    "@type": "GrantedLabel",
    "vcHash": {
      "hashType": "sha-256",
       "value": "xxxxxxxxxxxxxxxxxxxx"
    }
    "vc": {
      @id: "did:web:ovhcloud.provider.gaia-x.community:participant:xxx/located-service-offering/vc/xxx/data.json",
    },
    "label": {
      "hasName": "Gaia-X label Level 1",
      "@id" : "did:web:gaia-x.community:participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/733c76bea0ac1b3043b701f3ddb52ecb37c93844f481f030da823b9818e90d72/data.json"
    }
    "evidence": {
      "documentPresent": "digital",
      "subjectPresent": "digital",
      "type": [],
      "verifier": ""
    },
   "expirationDate": "2022-10-26T15:29:32.872Z",
   "issuer": "did:web:abc-federation.gaia-x.community2",
   "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2022-10-26T15:29:32.882456+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..taNLLHtr5IWgTgTBuJS1eS7uiZHs5KF_ogEjPVi7JXKaynKxrMKvg8_me7ecaWdRIgWKKfd4SY0caHta3bGlc4-ZoLFgAqKmCnom9zh9rFEOdPdhumDNOCyMAy6yjn37-4goaiTls2kz3NWGON0yH-g3kbd5mg6DmEV8JtAkA13QvBKBSjmwDdEwoAihqDxOsa-Dpqwe3N_DswBzvprcWzAARZVlOm8lkrMpBpqD9OvngaJxChGj1IQFnXT0LWwai-ENsCzkl9bsxAkFbWazJpRulBp_-Fz5YBjnNbXG1RqwrBU3ws5sxGSYNrwO7TcYHKqOEKfB2yuo3GOGI1GbmA"
}
```

In case of error : 
* Error http 400 bad request 
* content 
```
{
    "statut" : "KO"
    "unsatisfiedRules" : [
        'CPR_2' , 'CPR_4' 
    ]
}
```

## Test Rules Set 



