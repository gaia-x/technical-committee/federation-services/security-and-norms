

| Content | Link |
| ------ | ------ |
| Linked Data  | Berners-Lee, T. (2009). Linked Data. W3C. https://www.w3.org/DesignIssues/LinkedData |
| Cloud Federation | Bohn, R. B., Lee, C. A., & Michel, M. (2020). The NIST Cloud Federation Reference Architecture: Special Publication (NIST SP) - 500-332. NIST Pubs. https://doi.org/10.6028/NIST.SP.500-332 |
| eIDAS Trusted List | European Commission. Trusted List Browser: Tool to browse the national eIDAS Trusted Lists and the EU List of eIDAS Trusted Lists (LOTL). https://webgate.ec.europa.eu/tl-browser/#/ |
| Next Generation Cloud europe | European Commission. (2020). Towards a next generation cloud for Europe. https://ec.europa.eu/digital-single-market/en/news/towards-next-generation-cloud-europe | 
| Semantic Interoperability Community | European Commission Semantic Interoperability Community. DCAT Application Profile for data portals in Europe. https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe | 
| Technical Architecture | Federal Ministry for Economic Affairs and Energy. (2020). Gaia-X: Technical Architecture: Release - June, 2020. https://www.data-infrastructure.eu/GAIAX/Redaktion/EN/Publications/Gaia-X-technical-architecture.html | 
| framework for identity management | ISO / IEC. IT Security and Privacy – A framework for identity management: Part 1: Terminology and concepts (24760-1:2019(en)). ISO / IEC. https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en | 
| security policy | Oldehoeft, A. E. (1992). Foundations of a security policy for use of the National Research and Educational Network. Gaithersburg, MD. NIST. <https://doi.org/10.6028/NIST.IR.4734 https://doi.org/10.6028/NIST.IR.4734> | 
| licenses | Open Source Initiative. Licenses & Standards. https://opensource.org/licenses | 
| JSON-LD | W3C. JSON-LD 1.1: A JSON-based Serialization for Linked Data [W3C Recommendation 16 July 2020]. https://www.w3.org/TR/json-ld11/ |
| ODRL Information Model | W3C. ODRL Information Model 2.2 [W3C Recommendation 15 February 2018]. https://www.w3.org/TR/odrl-model/ |
| Verifiable Credentials |  W3C. Verifiable Credentials Data Model 1.0: Expressing verifiable information on the Web [W3C Recommendation 19 November 2019]. https://www.w3.org/TR/vc-data-model/ | 
| Semantic Web | W3C. (2015). Semantic Web. https://www.w3.org/standards/semanticweb/ | 
| DIDs | W3C. (2021). Decentralized Identifiers (DIDs) v1.0. https://www.w3.org/TR/did-core/ | 
| Schemas Gaia-x (deprecated) | https://sd-schemas.gaiax.ovh/wip/vocab/compliance/ | 
| schémas GXFS-FR | https://gitlab.com/stephane.teyssier/service-characteristics | 
| Tools around Schemas | https://gitlab.com/stephane.teyssier/service-characteristics/-/tree/develop/toolchain | 
| Compliance Model | https://gitlab.com/stephane.teyssier/service-characteristics/-/blob/develop/docs/wip/compliance.md | 

