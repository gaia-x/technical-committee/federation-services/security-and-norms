**THIS DOCUMENT IS DEPRECATED**
**This scope is managed by the Catalog **

 # Summary  
1. [Introduction](#introduction)
2. [References](#references)
3. [Glossary](#glossary)
4. [Principles](#principles)
5. [Data Models](#data-models)
6. [Technical View](#technical-view)

# Introduction

The goal of this document is to describe how a user can :
* Access a  __Digital Signature Service Portal__ displaying all __Digital Signature Services__ available to sign documents (json, pdf...)
* Use these __Digital Signature Services__ 
* Specify an optional Integration Component to simplify integration of any __Digital Signature Service__ 

Accessing these services allow a user to prove its identity in the [Compliance Service](./ComplianceService.md) or to provide a documenbt signed. 

This Digital Signature Services Portal will allow a user to sign some documents required by the [Scoring](https://gitlab.com/gaia-x/technical-committee/federation-services/security-and-norms/-/blob/main/ComplianceService.md).

This document cover specifications of components : 
* A __Digital Singature Services Portal__ to display Digital Signature Services for any user and without any authentication
* A __Management Portal__ to configure the __Digital Signature Service Portal__ 
* An __Integration Component__ to simplify integration between Gaia-x and Digital Trust Services

Digital Signature Services MUST be registered services in the federation catalog as extention of an extention of a Service Offering Object.

An Integration Component is provided to help Digital Signature Services to integrate existing services without any knowledge in new technologies (Verifiable Credential,Verifiable Presentation ...).

# References 

| Code | Link |
| ------ | ------ |
| W3C Verifiable Credential | https://www.w3.org/TR/vc-data-model/  |
| Gaia-x DID Auth | https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md |
| W3C VP request | https://w3c-ccg.github.io/vp-request-spec/ |
| Gaia-x Architecture| https://gaia-x.eu/sites/default/files/2022-05/Gaia-x%20Architecture%20Document%2022.04%20Release.pdf | 
| Gaia-x Ontology | https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/core/core.html |

# Glossary

| Term | Definition |
| ------ | ------ |
| Digital Signature Service | A  service provider is a legal entity providing and preserving digital certificates to create and validate electronic signatures and to authenticate their signatories as well as websites in general.  These service providers are qualified certificate authorities required in the European Union and in Switzerland in the context of regulated electronic signing procedures. A Digital Signature Service MUST be certified by eIDAS |
| Digital Signature Services Portal | Service (Web Page) displaying the list of services |
| Participant | A Participant is an entity, as defined in ISO/IEC 24760-1 as “item relevant for the purpose of operation of a domain that has recognizably distinct existence”, which is onboarded and has a Gaia-X Self-Description. A Participant can take on one or more of the following roles: Provider, Consumer, Federator. Section Federation Services demonstrates use cases that illustrate how these roles could be filled. Provider and Consumer present the core roles that are in a business-to-business relationship while the Federator enables their interaction. [source](http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/conceptual_model/#participants) |
|End User| An end User is a human participant |
| Issuer | an Issuer compliant with Gaia-x requirements |
| Integration Component | Component that simplifies existing services with the Gaia-x ecosystem |
| Verifiable Presentation (VP) | A set of one or more claims made by an issuer. A verifiable credential is a tamper-evident credential that has authorship that can be cryptographically verified. Verifiable credentials can be used to build verifiable presentations, which can also be cryptographically verified. The claims in a credential can be about different subjects. (Source W3C) | 
| Verifiable Credentials | A standard data model and representation format for cryptographically-verifiable digital credentials as defined by the W3C Verifiable Credentials specification [VC-DATA-MODEL]. |
| DID document | A set of data describing the DID subject, including mechanisms, such as cryptographic public keys, that the DID subject or a DID delegate can use to authenticate itself and prove its association with the DID. A DID document might have one or more different representations. |
| CSP | Cloud Service Provider |
| Candidate | A CSP is considered as a candidate if he is subscribing to the CSP suscribe page but has not completed the subscription process (completion of the form, verification of his declarations) and is therefore not yet registered. |
| User | A CSP is considered as a user if he has completed the CSP subscription process and is therefore registered. |
| DID | A DID is a type of URI scheme. verifiable credential. A standard data model and representation format for cryptographically-verifiable digital credentials as defined by the W3C Verifiable Credentials specification (W3C) |

# Integration Principles

Different integration facilities are provided to Digital Signature Services providers.
A Digital Signature Service Provider MUST use native and distributed authentication system or use the Federation Identity Provider (openid connector)

To publish a service, a Digital Trust Service Provider MUST publish as expected by the federation and the Gaia-x framework the service 
or use the OCM to publish it.

## Authentication 

The native one allow a Digital Signature Service to be found using the Digital Signature Service Portal and natively integrated with Distributed identities:

```mermaid
sequenceDiagram
  autonumber
  actor u as User
  participant portal as Digital Signature Services Portal
  participant gxu as Issuer 
  participant ts as Digital Signature Service 
u ->> portal: display Digital Signature Services
u ->> ts: redirect to a Digital Signature Service 
ts ->> u: request a Verifiable Presentation 
u ->> gxu: request a Verifiable Presentation with required fields 
gxu -->> u: return a Verifiable Presentation
u ->> ts: provide Verifiable Presentation from Issuer
note right of ts: User now loggued in Digital Trust Service 
```

| Flow id | Format | Description |
|---|---|---|
| 1 | GET url | return a web page displaying Digital trusted Services |  
| 2 | http redirect | the user get a  VP request as defined by [W3C](https://w3c-ccg.github.io/vp-request-spec/)  |  
| 3 | Verifiable Presentation Request | the service provide a VP request and the user have to get it [see DID Auth](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md) |
| 4 | Verifiable Presentation | the user provides the VP (json file ...) to the Digital Trusted Service as defined by[DID Auth](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md) | 
| 5 | Verifiable Presentation | the user has to provide the VP request to the issuer and get a Verifiable Presentation as defined by [DID Auth](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md)| 
| 6 | Verifiable Presentation | a valid token if the Verifiable Presentation is valid the user upload the Verifiable Presentation to the service the Verifiable Presentation | 

An other way for the Digital Trust Service provider is to use the protocol OpenID Connect provided by the Federation Identity Provider. This connector MUST provide a Verifiable Presentation in the JWT Token to allow the Digital trust Service Provider to get information about the user. 

To use for the first time the diagram will be this one : 

```mermaid
sequenceDiagram
  autonumber
  actor u as User
  participant portal as Digital Signature Services Portal
  participant ts as Digital Signature Service 
  participant idp as Federation Identity Provider 
u ->> portal: display Digital Signature Services
u ->> ts: redirected to the  Digital Signature Service using openid 
ts ->> idp: redirect to the idp to authenticate the user 
u ->> idp: user authentication using a VP 
idp ->> ts: return an authenticate user (token including a VC)
```

> TODO define the information in the VP (can we make a link to ComplianceService.md ??)

## Publish Service 

A Digital Signature Service Provider is a Gaia-x Service Offering and MUST be published as expected. 

A service is a Verifiable Presentation and MUST be accessible using any URI and published in the Federation Catalog to allow the Federation Catalog to index it. 

# Data models

## Class Diagram

```mermaid
classDiagram
  class VC
    VC: owner 
  class ServiceOffering 
      ServiceOffering: +String serviceName
      ServiceOffering: +String countryCode
      ServiceOffering: +String logo
      ServiceOffering: +Date registrationDate
      ServiceOffering: +String iso3166_country
      ServiceOffering: +String markdownDescription
  class language
   language: +String iso639_language
  class kindOfDigitalSignature 
    kindOfDigitalSignature: +String name
  class DigitalSignatureService
    DigitalSignatureService: +String urlService
  VC <|-- ServiceOffering
  DigitalSignatureService *-- language
  DigitalSignatureService *-- kindOfDigitalSignature
  ServiceOffering <|-- DigitalSignatureService
```

The serviceName is 'Digital Signature Service' and kindOfDigitalService is eSignature, ... to have more details.

A service to manage a list of services : 
| field |  type  | Description |
|---|---|---|
| name | string | eg eSignature, Archive ... | 

# Technical View 

## Components View

Components for this solution with their dependencies: 

```mermaid
flowchart TD
    portal("Digital Signature Service Portal") --> adss("API Digital Signature Service")
    mp("Digital Signature Services Management Portal") --> adss("API Digital Signature Service")
    adss --> fc("Federation Catalog")
    adss --> ge("Service end point")
    adss --> cws("OCM")
    mp("Digital Signature Services Management Portal") --> fidp("Federation Identity Provider")
    portal --> adss("API Digital Signature Service")
    style portal fill:#87CEFA
    style adss fill:#87CEFA
    style cws fill:#87CEFA
    style mp fill:#87CEFA
```
_in blue, components in scope of this document_

The components are in the scope of this documents: 
* Digital Signature Service Portal
* API Digital Trust Service
* Digital Trust Services Management Portal
* OCM API
* Storage

## Digital Signature Service Portal

Any End User (authenticated or not) can display a Web Page providing a list of Digital Signature Services

| Icon | Supplier | Service | Country | Languages | Gaia-x Label | Description |
|---|---|---|---|---|---|---|
| :bust_in_silhouette: | Supplier 1 | eSignature| France | fr,en | :star: :star: :star: | lorem lipsum |
| :cop: | Supplier 2 | eSignature | Germany | de | :star: :star: :star: | lorem lipsum |
| :cop: | Supplier 2 | Digital Wallet| Germany | de, en | :star: :star: | lorem lipsum |
| :guardsman: | Supplier 3 | Digital Wallet| Italy | it, en | :star: | lorem lipsum |

An End User may filter by
* Countries
* Supported languages
* Gaia-x label
* Kind of Service
* Supplier

The table by default is sorted by Gaia-x Level and registration date. 

The Web page is using the administration API to get services list. 

The portal support english, french and german. 

## Digital Signature Service Management Portal 

A Gaia-x User has some management screens and REST API : 
* Management (CRUD) of a list of Digital Trust Services (eSignature, Certificates ...). This service store only some meta datas and links to the Verifiable Presentation of the service.
* Some web pages to manage some lists: 
  * list of Countries
  * list of languages
  * list of kind of services
* OCM
  * allow to publish Verifiable Presentation of Digital Trust Services
  * allow to create / update did documents 

## APIs provided by API Digital Signature Service

API and Web pages are secured using DID Auth as expected [GXFS-fr Specifications for authentication](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md ) using an OpenID Connect protocol.

| Verb |  Path  | Rigths | format input | format output | Description |
|---|---|---|---|---|--|
| GET | /service/{id}| __any user__ without securities | n/a | format Service |  GET to retrive datas about the service {id} | 
| GET | /service | __any user__ without securities | request parameters  | list of Digital Trust Services | GET a list of services and allow using parameters to filter results |
| POST | /service/ | participant id logued and = participant id of the service | format service | format service  | POST to create a new service |
| PUT | /service/{id} | participant id logued = participant id of the service | format service | format service | PUT to create or update a service |
| DELETE | /service/{id} | participant id logued = participant id of the service | n/a | n/a | delete a service (deactivate it) |
| GET | /referential/{ref} | only Gaia-x Digital Trust Service Administrators | n/a | json stored for the referentiaL {ref} | |
| PUT | /referential/{ref} | only Gaia-x Digital Trust Service Administrators | any json for the key {ref} | PUT to create or update a referential | 
| DELETE | /referential/{ref} | Gaia-x Digital Trust Service Administrators | n/a | n/a | delete a referential  for the key {ref} | 

The goal of the referential API is to store some common datas like /referential/kindofSignatureService/ to get a list of services like eSignature, Certificates ... 
APIs to manage a service MAY be provided by the federation catalog (depend of the availability) and to get details follow the link to the VP provided by the catalog. 

>quelle place pour un role admin gaiax (ou non) ? 

## OCM API

This component MAY be reused and it's not required. Services MAY use any compliant solution.  

| Verb |  Path  | Rigths | format input | format output | Description |
|---|---|---|---|---|---|
| GET | /vp/{id}/.well-known/did.json | __any user__ without securities | n/a| format service | Service used for DID resolution as defined by the W3C |
| GET | /vp/{id}/ | __any user__ without securities | n/a| format service | Service that may be used to get the VP |
| PUT | /vp/{id}| participant logued = vp participant | n/a| format service | create or update a verifiable presentation. If a VP exists, the participant calling this APIs MUST be the same as the VP owner. A VP MUST be updated only by the creator of a service | 
| DELETE | /vp/{id}| participant logged = vp participant | n/a| format service | a service MUST be deleted only by the owner of a service | 
| POST | /service/{serviceId} | participant logued = vp participant | n/a | http redirect |  post the VC included in the token to the {serviceId} using the url configured in the service {urlSend}  and send to the requester the url to redirect the user |  
| GET | /did/{did}/ | __any user__ without securities | n/a | did document  | service to acces to the public key of the participant publishing a Digital Trust Service |
| GET | /did/{did}/.well-known/did.json | __any user__ without securities | n/a| did document  | service to acces to the public key of the participant publishing a Digital Trust Service |
| PUT | /did/{did}/ | __any user__ without securities | n/a| did document | service to create or update to the public key of the participant publishing a Digital Trust Service |
| DELETE | /did/{did}/ | __any user__ without securities | n/a| did document | service to create or update to the public key of the participant publishing a Digital Trust Service |


all informations MUST be extracted from the Verifiable Presentation except :
* url of the Verifiable Presentation
* markdownDescription
* description 
Informations are extracted and stored to allow an API to search/filter/order results within the portal

This service is storing and retrieving VP as provided by the supplier and signed by the supplier. This service MUST NOT check information. 

## API Digital Signature Service

sample of Digital Signature Service: 
```javascript
{
  "@context": "https://gaiaix/digitalsignatureservice.jsonld",
  "@id": "did:web:https://cloudprovider.eu/vp_storage.js",
  "urlVerifiablePresentation" : "https://cloudprovider.eu/vp_storage.js",
  "kindOfTrustSerrvice": "eSignature",
  "labelLevel": 1,
  "countryCode": "fr",
  "languagesCode": "fr,en,it",
  "supplierName": "cloudprovider",
  "markdownDescription": "a __trusted__ service", 
  "description" : "a trusted service",
    "urlService": "https://ocm.gaia-x.eu/createContract.do/"
}
```

In the request url, some parameters allow to filter services: 

| Parameter |  Type  | Description |
|---|---|---|
| country-code | string | country code to 
| lang | string | |
| label | string | |
| kindOfService | string | |
| didSupplier | string | did of the supplier|
| countMax | int65 | return max countMax items |
| page | int64 | return page 1,2 .. |
| sort | string | list of fields to sort the result sort=lang:asc,label:desc |  

This url dts.gaiax.eu/?lang=fr&sort=label:asc will filter to services using french and sorted by label level. 

List of services format:

```javascript 
{
     "items" : [ 
        { /* format Service */ }, 
        { /* format Service */ }
     ]
}
```

This API is done calling other API from the Federation Catalog and direct call to the service description.

## Digital Signature Services

The Digital Signature Service MUST provide a web page integrated with the openid connector or using VC/VP to authenticate the user as specified by Gaia-x Trust Framework. 

A Digital Signature service MUST sign the Verifiable Credential (or Presentation?) with a certificate (eIDAS or not) and provide the public key for anyone to be able to verity (?).
