[Table of Contents](#table-of-contents)
- [1. References](#1-references)
- [2. Glossary](#2-glossary)
- [3. Introduction](#3-introduction)
- [4. Service Description Wizard tools](#4-service-description-wizard-tools)
  - [4.1. Wizard \& API to create a service](#41-wizard-api-to-create-a-service)
  - [4.2. Claims](#42-claims)
  - [4.3. Documents required table](#43-documents-required-table)
  - [4.4. Notarization](#44-notarization)
- [5. Compliance and Labelling](#5-compliance-and-labelling)
  - [5.1. Compliance checking Process](#51-compliance-checking-process)
  - [5.2. Compliance Participant](#52-compliance-participant)
  - [5.3. Compliance Located Service Offering](#53-compliance-located-service-offering)
  - [5.4. Labelling](#54-labelling)
    - [5.4.1. Contractual framework](#541-contractual-framework)
    - [5.4.2. Data Protection](#542-data-protection)
    - [5.4.3. Cybersecurity](#543-cybersecurity)
    - [5.4.4. Portability](#544-portability)
    - [5.4.5. European Control](#545-european-control)
    - [5.4.6. Criterion](#546-criterion)
      - [5.4.6.1. Self-assed claims](#5461-self-assed-claims)
      - [5.4.6.2. Self-declared claims](#5462-self-declared-claims)
      - [5.4.6.3. Claims reference to CAB's Claims](#5463-claims-reference-to-cabs-claims)
  - [5.5. Trust Anchor](#55-trust-anchor)
    - [5.5.1. Automated exchange with official certification lists](#551-automated-exchange-with-official-certification-lists)
    - [5.5.2. Link to automated supporting document analysis services](#552-link-to-automated-supporting-document-analysis-services)
    - [5.5.3. Digital Signature Service](#553-digital-signature-service)
    - [5.5.4. Manual verification of a document (claim)](#554-manual-verification-of-a-document-claim)
    - [5.5.5. Receipt of Verifiable Credentials from organizations](#555-receipt-of-verifiable-credentials-from-organizations)
- [6. Issuing Verifiable Credentials or Verifiable Presentation](#6-issuing-verifiable-credentials-or-verifiable-presentation)
  - [6.1. VC Issuer](#61-vc-issuer)
    - [6.1.1. Goal](#611-goal)
    - [6.1.2. Input Data](#612-input-data)
    - [6.1.3. Deliverables](#613-deliverables)
  - [6.2. Repositorty for a participant](#62-repositorty-for-a-participant)
    - [6.2.1. Goal](#621-goal)
    - [6.2.2. Deliverables](#622-deliverables)
- [7. Service lifecycle management](#7-service-lifecycle-management)
- [8. Components View](#8-components-view)
  - [8.1. API](#81-api)
  - [8.2. Formats](#82-formats)
    - [8.2.1. Format Verifiable Credential Participant (Provider)](#821-format-verifiable-credential-participant-provider)
    - [8.2.2. Format Verifiable Credential Participant](#822-format-verifiable-credential-participant)
    - [8.2.3. Format vc Compliance](#823-format-vc-compliance)
    - [8.2.4. Format Verifiable Presentation Service](#824-format-verifiable-presentation-service)
    - [8.2.5. SecNumcloud Claim](#825-secnumcloud-claim)
    - [8.2.6. X509 Claim](#826-x509-claim)
    - [8.2.7. ISO 27001](#827-iso-27001)
    - [8.2.8. Format VC Terms and Conditions](#828-format-vc-terms-and-conditions)
    - [8.2.9. Format VC Terms and Conditions](#829-format-vc-terms-and-conditions)

# 1. References

| Code | Link |
| ------ | ------ |
| W3C Verifiable Credential | https://www.w3.org/TR/vc-data-model/  |
| Gaia-x Frameworks and documents | https://docs.gaia-x.eu/framework/ |
| Gaia-x DID Auth | https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md |
| W3C VP request | https://w3c-ccg.github.io/vp-request-spec/ |
| Gaia-x Architecture| https://gaia-x.eu/wp-content/uploads/2022/06/Gaia-x-Architecture-Document-22.04-Release.pdf | 
| Gaia-x Trust Framework | https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/ | 
| Gaia-X Labelling Framework | https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf |
| Gaia-X Labelling Criteria | https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X-labelling-criteria-v22.04_Final.pdf |
| Gaia-X Policy Rules Document | https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X_Policy-Rules_Document_v22.04_Final.pdf |
| Gaia-x Ontology | https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/core/core.html |
| DID | https://gitlab.com/gaia-x/technical-committee/federation-services/self-description-model/-/blob/main/docs/did.md |
| Ontology | https://schemas.abc-federation.gaia-x.community/ |

# 2. Glossary

| Term | Definition |
| ------ | ------ |
| Digital Signature Service | A dignatal service provider is a legal entity providing and preserving digital certificates to create and validate electronic signatures and to authenticate their signatories as well as websites in general. these service providers are qualified certificate authorities required in the European Union and in Switzerland in the context of regulated electronic signing procedures. These services MUST be certified by eIDAS |
| Digital Signature Services Portal | Service (Web Page) displaying the list of trusted services |
| User | Any user like End User or Service Provider |
| Issuer | an Issuer is signing documents and compliant with Gaia-x requirements |
| CSP | Cloud Service Provider |
| Candidate | A CSP is considered as a candidate if he is subscribing to the CSP suscribe page but has not completed the subscription process (completion of the form, verification of his declarations) and is therefore not yet registered. |
| Auditor (CAB) | An auditor is a person or firm that conducts an independent examination of an organization's financial records and statements to ensure they are accurate and comply with relevant laws and regulations |
| SD | Service Description as defined by Gaia-x |

# 3. Introduction 

The scope of this specification may be descripted with this schema:
![image](Assets/ComplianceProcessV2.png)

A Service Provider MUST provide a Verifiable Presentation for a service to publish it with a Gaia-x Label ([like this sample](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/main/src/tests/provider-instance-test.json))

The Service Provider MUST upload required documents to get a label as defined by the [Gaia-x Labelling framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf). 
The Compliance Service for Services or Participants require only Verifiable Credentials.

If documents are not provided as Verifiable Credentials, Service Providers MAY use a Notarization component to provide some documents and get a Verifiable Credential. Documents MAY be : 
* a certification/claim as pdf (PDF/a) 
* a certification as Verifiable Credential (from a trusted auditor or issuer) included in the Verifiable Presentation of a Service Offering.
* use a valid eIDAS Certificate to sign Verifiable Credential with a valid DID

The Rule-Checker Service provides 3 services:
* checking if a Provider is Gaia-x compliant as defined by **Gaia-x Compliance** 
* checking if a Located Service Offering** is Gaia-x compliant as defined by **Gaia-x Compliance**
* provide a label for a Located Service Offering as define by **Gaia-x Labelling** 

The Compliance  perform checks as defined in the [trust framework](http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors):
* syntactic correctness. 
* schema validity.
* cryptographic signature validation (for all signatures and a full check (a valid certificate, valid root ...).
* attribute value consistency.
* attribute value verification.
* compliant with schema (SHACL) from Gaia-x Ontology.

The Labelling calculate the label (level 1, level 2...) for a located service offering using Gaia-x Labelling Rules. 

The Rules Checker provide Verifiable Credentials signed by the Federation for Compliance and Labelling.

Some links to Participants to some Digital Signature Services to prove Participant's identity:
* services to get an eIDAS certificate
* sign a document with a trusted certificate

The rules-checker MAY store the Verifiable Presentation and MUST store Verifiable Claims and revokations associated.

# 4. Service Description Wizard tools 

## 4.1. Wizard & API to create a service 
Wizard Tool is the entry point for service providers to describe their services and thus to create their related gaia-x object with respect to ontology. 
Wizard Tool can be seen as a tool to help service providers creating their VPs and VCs which are mandatory to publish them to the Federated Catalogue with compliance requirements and Gaia-X label. Thanks to Wizard Tool any service provider company already onboarded can visualize its services on this web interface.

It is a user friendly web interface for each company represented by an authorized and specific user. Once the user is connected to the wizard tool dedicated to his company he can fill out all the service fileds mandatory (Service Description) per service he wants to be registered to the federated catalogue.

> NB: Authentication is out of the scope for November 2022 due to lack of time of integration with OCM/PCM component)

The functionnalities of the Wizard Tool can be summmarized in 8 steps :
1. Service description creation
2. Certification(s) loading
3. Questionnaire and self-assessed criterias response
4. Label simulation
5. VP creation and signature
6. Compliance request
7. Label request
8. Service publication on the federated catalogue

**Service description creation**

To create a service user has to fill in several fields in a form on the web interface. For instance :
|Fields|Description|Example|
|--|--|--|
|Name of the service|a simple string field|i.e "my Service"
|Description of the service|a simple text field| i.e. "description of my service"
|Type of the service|a simple radio button to check among already known service type|i.e. Authentication, Backup, Compute, Digital Signature, Storage...
|Layer|a simple radio button to check among already known layer|i.e. IAAS, PAAS, SAAS|
|Location|the location of the service|i.e. Grenoble in France, Milano in Italie, Munchen in Germany...
|Compliance References|the certifications of the service|i.e. ISO27001, SecNumCloud, CISPE...|
|Questionnaire|several questions about criterias met by the service|self assessed criterias|
|...|...|...|

Some fields and attributes are dependent on other components and ontology like Location, Participant, ServiceOffering, Provider... To do so, the Wizard Tool may call these services trough APIs.

**Certification(s) loading**

Since auditors or certifications providers do not yet issue certifications in VC format properly interpreted in gaia-x, user must use a Notarization Service to transform his signed PDF certification documents into VCs. That's why Wizard Tool have to call Notarization API to do so, thanks to a pop-up allowing to drag and drop PDF.

For instance, a call to notarization with a json object as argument including name of the participant, certification type(s), base64 encoded string of the pdf document(s) and the _ServiceOffering_ object being used can be done to receive back by Notarization a new object _LocatedServiceOffering_ which is the Verifiable Credential. This VC is an gaia-x object whose claims can be  signed by the federation or gaia-x.

**Questionnaire and self-assessed criterias response**

Some criteria related to label attribution can be self-assessed, that's why user might fill out a questionnaire with three categories of questions (Contractual Governance, Transparency, European Control). Answers are included thanks to the Notarization to an object contained on the VC _LocatedServiceOffering_.

**Label simulation**

So that the user can know which label his service will be eligible for, a simulation button could be created. It allows user to send the previously obtained VP and to know the label. It's only a simulation, the compliance service is not called and no compliance VC is returned.

**VP creation and signature**

VP has to be signed in order to be sent. Signature is out of scope of Wizard Tool for November 2022. It has to be done with an external service.
That's why user can download it in order to sign it (it should be done by PCM).
```
Download VP on Wizard Tool :
//click and get "vp_unsigned.json" file

Signature with agent thanks to curl :
curl -X PUT -H "Content-Type: application/json" -d '@vp_unsigned.json' https://vc-issuer.dufourstorage.provider.gaia-x.community/api/v0.9/sign > vp_signed.json
//received "vp_signed.json" file
```
Then user has to upload his VP signed from his computer to the wizard tool. Wizard Tool stores his VP on agent (like a PCM service).

**Compliance request**

The VP containing _LocatedServiceOffering_ VC is then presented to compliance service in order to have a compliance VC object. User has just to click on a button to do this action. If compliance returns an HTTP 2XX, VP seems to be valid, and Wizard Tool change service status on the web page.
The _ComplianceObject_ VC received is concatenated to the previous VP.

**Label request**

VP composed with two previous VCs is finally presented to labelling service in order to have a label associated with the service. User has just to click on a button to do this action. If labelling service returns an HTTP 2XX, VP seems to be valid, and Wizard Tool change service status on the web page.
The _GrantedLabel_ VC received is concatenated to the previous VP.
Thanks to previous stepts, VP is now composed of three kind of VC (_LocatedServiceOffering_, _ComplianceObject_ and _GrantedLabel_) as shown below :

|VP Initial|VP After Compliance|VP After Labelling|
|--|--|--|
|LocatedServiceOffering VC|LocatedServiceOffering VC|LocatedServiceOffering VC|
|/|ComplianceObject VC|ComplianceObject VC|
|/|/|GrantedLabel VC|

**Service publication on the federated catalogue**

Final step is to publish the VP the the federated catalogue in order to expose the service with its related compliance and label. 

## 4.2. Claims

Clamais MUST be integrated into a Verifiable Pressentation and POST it to the Rules Checker Service.

In particular, there are several proofs that can be provided:
* VC signed with a private key linked to a X.509 certificate
  * the certificate has to be recognized as trusted (eIDAS)
  * VC claims must correspond to certificate's extensions (especially Subject DN)
    * country : C= in Subject DN
    * organization : O=
    * registrationNumber : organizationIdentifier (OID.2.5.4.97)
    * city : L= 
  * the verification service verifies the values of certificate against VC claims
* VC containing a signed document (PAdES) or a link to the document
  * VC is self signed with any key, this information is ignored
  * VC includes the document in base 64 or a link to the document and a hash corresponding to the document, in this last case, the hash must be verified.
  * Document signature verification must be made as a first start of verification, including certificate validity.
  * VC claims must correspond to certificate's extensions (especially Subject DN) [same verifications as previous §]
    * country : C= in Subject DN
    * organization : O=
    * registrationNumber : organizationIdentifier (OID.2.5.4.97)
    * city : L= 
  * information can be updated trustfuly with data included in the document, that are considered as self declaration 
  * this format can be extended to other formats that are machine readable (XAdES, JAdES).

## 4.3. Documents required table

* Required documents table (linked with the PRD document) by certification
* Document upload, Link upload (VC) (+API)

For the Label, the list of proof by Criterion

* Contractual governance
    * C1, C2, C3, C4 <- self-assessment through the trust framework
* Transparency
  * C5, C7, C8, C9, C10, C11, C12, C13 <- self-assessment through the trust framework
  * C6 <- self-assessment through the trust framework An accepted standard is ISO19944
  * C14 <- Gaia-X trust framework checking the self-description (existence of a Policy in SD)
  * C15 <- Gaia-X trust framework checking the self-description (existence of a verified Identity in SD)
  * C16 <- Gaia-X trust framework checking the self-description (conformant SD)
  * C17 <- Gaia-X trust framework checking the self-description (consistent attribute)
  * C18 <- Gaia-X trust framework checking the self-description (consumer has identity provided by te federator) ??
* Data Protection
  * C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C31  <- L1 self verified, L2/L3 (accepted standard codes of conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc.Art. 42 GDPR.)
* Security
  * C32 -> C51  <- Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA
* Portability
* C52, C53 -> Accepted Standards: SWIPO IaaS, SaaS and merged code CoC
* C54, C55 -> Gaia-X self-declaration through the trust framewor
* C56-> C61 -> Gaia-X self-declaration

Compliance criterion are very close to the label criterion

[TrustFramework validates these type of rules](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/#gaia-x-trust-framework_1):
* serialization format and syntax. 
* cryptographic signature validation and validation of the keypair associated identity.
* attribute value consistency.
* attribute veracity verification.

* Priority criterion (from lower priority to higer priority)
  1. verification of the Self-description format (schema validator)
  2. Self declared claims
  3. Claims validated by an independant organisation. 
* For November: CISPE, ISO 27001, SecNumCloud (in that order)

## 4.4. Notarization 

The aim of the notarization service is to issue VCs of certifications on behalf of auditors or certification issuers who are not yet able to issue VCs. Since auditors or certifications providers do not yet issue certifications in VC format properly interpreted in Gaia-x, Notarization Service will be called to transform current signed PDF certification documents into VCs.

We can therefore summarize the role of notarization in a component that makes it possible to transform signed pdf documents of proof of certification into VC.

**Trusted List**

To be able to verify pdf signatures, the Notarization Service may update its european trusted lists with API calls, for instance : 
 - European Trusted List : https://ec.europa.eu/tools/lotl/eu-lotl.xml
 - EU Official Journal Trusted Certificates : https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.C_.2019.276.01.0001.01.ENG
 - Trusted Certificates of Trusted Registry of the Federation

All these credentials are supposed to be trusted, trusted list for certification and trusted registry for self assessment.

**VC Creation (certification)**

First step is the creation of the VC _LocatedServiceOffering_ asked by a service provider.

After having filled out service description for a given service, _Wizard Tool_ is supposed to call Notarization. A call is done when when user drags and drops its signed pdf related to his certification.

During this step Notarization has to create and use some ontology objects such as :
 - _ComplianceReference_ related to certification,
 - _ThirdPartyComplianceCertificationScheme_,
 - _ComplianceAssessmentBody_,
 - _ThirdPartyComplianceCertificateClaim_,
 - _ThirdPartyComplianceCertificateCredential_,
 - _LocatedServiceOffering_,

The Notarization store all Verifiable Credentials created (through API, i.e. user agent or federation or catalogue).

**VC Creation (self assessment)**

Some criteria related to label attribution can be self assessed, that's why user might fill out a questionnaire (can be dne via Wizard Tool). Answers must have to be concatenate to the VC LocatedServiceOffering previously issued.


# 5. Compliance and Labelling

Compliance is the set of rules and regulations with which a participant, service or location must comply. It can relate to different areas. Compliance with these rules is essential to ensure the legality of the participant, service or location.  

Labeling is the trusted reference appearing on the services intended for users. Its purpose is to provide the user with clear and precise information on the characteristics and properties of the service. Labeling must be defined by international regulations. 
 
If compliance is validated, a verifiable credential attesting to this will be issued to you. VC compliance is mandatory to be part of gaia-x. If you meet the labeling criteria, you will be issued a verifiable credential attesting to this, having a label is strongly recommended but it is not mandatory.

The Compliance Service validate the shape, content and credentials of Self Descriptions and issues a Verifiable Credential attesting of the result. Required fields and consistency rules are defined in the [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/).

## 5.1. Compliance checking Process

The purpose of the compliance and all the process around is to validate the consistency and veracity of data in input. It could be Verifiable Presentation for Located Service Offering, or Verifiable Credential for Participant and Provider.  

First of all, consistency of attributes. It means that the input has to be compliant with a specific context and type. A valid Participant will share keys in common with the [Participant shape](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/#legal-person)  

Secondly, checking the consistency of proofs. The Verifiable Credential like the Verifiable Presentation needs to be signed by a proof. Those proof has to present a [Proof Format](https://www.w3.org/TR/vc-data-model/#proof-formats), this is related to the W3C.  

Finally, the compliance process checks if the entity’s issuer is valid and a trusted entity. Also the proof has to be listed as Accepted Standards by GAIA-X and the reference is valid (as for any criterion in labelling, described in https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X-labelling-criteria-v22.04_Final.pdf we have a verifying entity and an accepted standard)  

If all the requirements are met, this service will create a Verifiable Credential related to the [gax-compliance:ComplianceObject](#823-format-vc-compliance) and send to the [VC issuer](#61-vc-issuer) to get a signed VC. 

## 5.2. Compliance Participant  
  
The Compliance Participant check if the participant verifiable credential is compliant with all technical Gaia-x requirements. 

[Here](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/#legal-person) is the reference of participant object : https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/#legal-person  

The table below show all the rules provided by Gaia-X for the participant VC. Each of these rules must be validate.

| Rule name | Description |
| --------- | ----------- |
| CPR-01_checkJsonLd | Check json-ld validity |
| CPR-02_checkSchema | Check gaia-x schemas (TODO mettre le lien du schema)  gax-core and gax-participant used and verified |
| CPR-03_checkSignature | Check signatures validity as defined by W3C |
| CPR-04_checkRegNum | If legalAddress.country is located in the European Economic Area, Iceland, Lichtenstein and in Norway, then registrationNumber must be at least one of the following types: EORI, leiCode, local, vatID. To check the value of these types, please use the APIs: https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/trust_anchors/#for-registrationnumberissuers-trust-anchors. If more than one type is registered, check the consistency of the informations. |
| CPR-05_checkUSState |  If legalAddress.country is located in United States of America, than a valid legalAddress.state using the two-letter state abbreviations is mandatory |
| CPR-06_checkLeiCodeInHQuarter | leiCode.headquarter.country shall equal headquarterAddress.country. |
| CPR-07_checkLeiCodeInLegAddress | leiCode.legal.country shall equal legalAddress.country. |
| CPR-08_checkDid | All DID are resolvable using a did resolver |
| CPR-09_checkTracability | The keypair used to sign the Data Resource claims must be traceable to the producedBy participant of the Data Resource. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |
| CPR-10_checkContainsPII | If containsPIIis true, attributes related to the Legitimate Processing of Information related to PII become mandatory. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |
| CPR-11 | The role of a participant can be supplier, consumer or federator. |
| CPR-12 | The location of a participant can be outside of Europe and outside of the USA |

:exclamation: Checking the registrationNumber require an API.

## 5.3. Compliance Located Service Offering 
  
The Compliance Located Service Offering check if the Service offering verifiable credential is compliant with all technical Gaia-x requirements.  

[Here](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/service/#service-offering) is the reference of service offering object : https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/service/#service-offering  

Like the Compliance participant rules table, this one is about service offering rules provided by Gaia-X for the located service offering VC. Each of these rules must be validate.

| Rule name | Description |
| --------- | ----------- |
| CSR-01_checkJsonLd | Check json-ld validity |
| CSR-02_checkSchema | Check gaia-x schemas (TODO mettre le lien du schema) gax-core and gax-service used and verified |
| CSR-03_checkSignature | Check signatures validity as defined by W3C |
| CSR-04_checkHttpCode | All URI provided MUST return HTTP CODE 2xx on a http call |
| CSR-05_checkVCProvider | Provider participant MUST have a "complianceCredential" VC |
| CSR-06_checkDid | All DID are resolvable using a did resolver |
| CSR-07_checkClaims | All claims need to be verified |
| CSR-08_checkTracability | The keypair used to sign the Data Resource claims must be traceable to the producedBy participant of the Data Resource. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |
| CSR-09_checkContainsPII | If containsPIIis true, attributes related to the Legitimate Processing of Information related to PII become mandatory. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |
| CSR-10 | The keys used to sign a SERVICE OFFERING description and the providedBy PARTICIPANT description should be from the same keychain. |
| CSR-11 | The output of the “contractNegotiationEndpoint” shall point to the result of the negotiation signed by all the Participants in direct link with the negotiation. |

## 5.4. Labelling

Gaia-X Labeling is a compliance and labeling technology process that automates all the checks necessary to give a service a specific label so that users can make decisions and information is verified or verifiable. Gaia-X labeling is thus defined as the process of examining and validating the criteria that are met or not met.

Each label is define by a set of criterion, to validate label n your service have to validate label n-1.
List of criterion to validate labels : https://gitlab.com/gaia-x/policy-rules-committee/label-document/-/blob/master/docs/policy_rules_labeling_document.md

| Label level | Description | link |
| --------- | ----------- | ---------- |
| 1 | Data protection, transparency, security, portability, and flexibility are guaranteed in line with the rules defined in the Gaia-X Policy Rules Document and the basic set of technical requirements derived from the Gaia-X Architecture Document. For cybersecurity, with the minimum requirement being to meet ENISA’s European Cybersecurity Scheme - Basic Level. | [Label 1](https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json) |
| 2 | This advanced Label Level 2 extends the basic requirements from Level 1 and reflects a higher level of security, transparency of applicable legal rules and potential dependencies. The option of a service location in Europe must be provided to the consumer. Regarding cybersecurity, the minimum requirement will be to meet ENISA European Cybersecurity Scheme - Substantial Level. | [Label 2](https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/8a9bae461da3b6945ece65b342fe14093509eda4494f5b5116a14d64736211b5/data.json) |
| 3 | This level targets the highest standards for data protection, security, transparency, portability, and flexibility, as well as European control. It extends the requirements of Levels 1 and 2, with criteria that ensure immunity to non-European access and a strong degree of control over vendor lock-in. A service location in Europe is mandatory. For cybersecurity, the minimum requirement will be to meet ENISA’s European Cybersecurity Scheme - High Level. | [Label 3](https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/3dddebee83fef07bc13c24537544c1ce1df1875a01a281413551c1e2b743611a/data.json) |
  
### 5.4.1. Contractual framework
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Contractual governance | 1.1.x | Self-assessment through the trust framework |
| General material requirements and transparency | 1.2.x | Self-assessment through the trust framework |
| Technical compliance requirements | 1.3.x | Self-assessment through the trust framework |
  
### 5.4.2. Data Protection
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| General | 2.1.x | Label 1: self-verified through internal audit according to an approved CoC/certification scheme and signed Gaia-X self-declaration<br />Label 2 / Label 3: evaluation by monitoring or third party; certification: inspection/verification/validation based on audit by CAB |
| GDPR Art. 28 | 2.2.x | Label 1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. <br />Label 2 / Label 3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body. |
| GDPR Art. 26 | 2.3.x | Label 1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.<br />Label 2 / Label 3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body. |
  
### 5.4.3. Cybersecurity
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Cybersecurity | 3.1.x | Label 1: internal audit; externally confirmed to be following recognized standards and/or good practices<br />Label 2: onsite assessment following assessment process according to the respective standards<br />Label 3: According to process for EUCS Level High ; ad interim: see Label Level 2 |

### 5.4.4. Portability
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Switching and porting of Customer Data | 4.1.x | Label 1 & Label 2: self-verified through internal audit and signed Gaia-X Self-Declaration<br />Label 3: SWIPO self-declaration |

### 5.4.5. European Control
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Processing and storing of Customer Data in EU/EEA | 5.1.1 | Label 2 : Self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL |
| Processing and storing of Customer Data in EU/EEA | 5.1.2 to 5.1.7 | Label 3 : Self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL |
| Access to Customer Data | 5.2.1 | Gaia-X Self-Declaration |

To see more details on Crierion: https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/22.11/policy_rules_labeling_document/#gaia-x-policy-rules-and-labelling-criteria-for-providers
 
### 5.4.6. Criterion

[ComplianceLabel](#54-labelling) objects are declared by Gaia-x.   

##### ComplianceLabel ontologie
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasName | string | //// |
| hasDescription | string | //// |
| hasLevel | string | //// |
| hasRequiredComplianceCriteriaCombination | List<ComplianceCriterion> | //// |


To complete a label, you must obtain or declare all the **Compliance Criterion** found in "**hasRequiredComplianceCriteriaCombination**" and also complete all previous label levels.
Example: For label 2 you need label 1 and label 2 criterion.

A Criterion is defined by it's "**ComplianceCriterion**" object.
With the attribute "**canBeSelfAssessed**" we can know all criteria that can be found in the wizard questionnaire.

##### ComplianceCriterion ontologie
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasName | string | //// |
| hasDescription | string | //// |
| hasLevel | string | //// |
| hasCategory | string | //// |
| canBeSelfAssessed | boolean | //// |

URL to find all the criterion : https://federated-catalogue-api.abc-federation.dev.gaiax.ovh/api/compliance_criterions

#### 5.4.6.1. Self-assed claims

Criteria validated by the answers to the questionnaire : 1.1.1 to 1.3.5 and 5.1.1 to 5.2.1

##### SelfAssessedComplianceCriteriaClaim ontologie
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasServiceOffering | ServiceOffering | //// |
| hasAssessedComplianceCriteria | List<ComplianceCriterion> | //// |

#### 5.4.6.2. Self-declared claims

The self-declared claims come from certifications that are self-signed by the provider and do not have any corresponding claims from a Certification Authority (CAB) (see Claims reference to CAB's Claims).
Notarization associates each claim with its corresponding criterion and stores it in the "**grantsComplianceCriteria**" field. The criteria in this field must be combined with those in the same field in the schemas coming from the **ThirdPartyComplianceCertificationScheme**.

##### ComplianceCertificationScheme ontologie
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasComplianceReference | ComplianceReference | //// |
| grantsComplianceCriteria | List<ComplianceCriterion> | //// |

#### 5.4.6.3. Claims reference to CAB's Claims

Those are claims signed by a provider correspond to one or more claims posted and signed by a CAB. The claims signed by a trusted third party validate the higher-level criteria.

##### ThirdPartyComplianceCertificationScheme ontologie
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasComplianceAssessmentBodies | List<ComplianceAssessmentBody> | //// |
| hasComplianceReference | ComplianceReference | //// |
| grantsComplianceCriteria | List<ComplianceCriterion> | //// |

## 5.5. Trust Anchor  

This service provided by the component Trust Anchor and specified in the ICAM Working Group.
This service store a list of trusted root certificates as defined by the Gaia-x Framework.
This service is able to check if a certificate is trusted or not based on a list.
This component is required for the labelling process.

### 5.5.1. Automated exchange with official certification lists 

Servies to provide:
* eiDAS	Issuers of Qualified Certificate for Electronic Signature as defined in eIDAS Regulation (EU) No 910/2014
  (homepage: https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home)
  (machine: https://ec.europa.eu/tools/lotl/eu-lotl.xml)
* Moke up for November:
    * a service to retreive a scoped certificates list by an [LEI Code](https://schema.org/leiCode) as described by [GLEIF](https://www.gleif.org/en/about-lei/get-an-lei-find-lei-issuing-organizations)

Out of scope for November:
* Documentary analysis for extracting relevant information, example : analysis of AFNOR certificate to extract the company, date, type of certificate
* Certification validation systems list table
* Exchange format with certification validation systems
* Exchanged flows ( Inputs/Outputs, etc.)
* gleif	List of registered LEI issuers.
(homepage: https://www.gleif.org/en/about-lei/get-an-lei-find-lei-issuing-organizations)
(machine: https://api.gleif.org/api/v1/registration-authorities)
* State	The Trust Service Providers (TSP) must be a state validated identity issuer.
  - For participant, if the legalAddress.country is in EEA, the TSP must be eiDAS compliant.
  - Until the end of 2022-Q1, to ease the onboarding and adoption this framework, DV SSL can also be used.
  - Gaia-X Association is also a valid TSP for Gaia-X Association members.
* EDPB CoC	List of Monitoring Bodies accreditated to the Code of Conduct approved by the EDBP
(list of EDBP’s CoC: https://edpb.europa.eu/our-work-tools/documents/our-documents_fr?f%5B0%5D=all_publication_type%3A61&f%5B1%5D=all_topics%3A125)

### 5.5.2. Link to automated supporting document analysis services

Manual Document Analysis is __Out of scope__ for november 2022. 

In some future release above November 2022:
* Supporting document analysis service directory by document type
* Content description and mockup of the service directory page by document type 
* Supporting document  analysis service description and subscribe standard pages (+ link to the API catalog by service)

### 5.5.3. Digital Signature Service

A Digital Signature Service allows:
* to get eIDAS certificates
* to sign documents 

Digital Signature Services are availble thru the catalog 

Certificates eIDAS are required to sign a Verifiable Presentation to register any Located Service Offering in Gaia-x Target. If a participant have not yet an eIDAS certificate, the portall allows it to find a provider.
The other option for a participant is to use an eSignature service to sign a document and add it in the Verifiable Presentation.

```mermaid
flowchart TB
  id1((start))
  id2{certificate from <br/>an authority recognized by EU ?}
  id1-->id2
  id3[[Sign a VP with it]]
  id2-- yes ---id3
  id4[[Use a Digital Signature Service ]]
  id2-- no ---id4
  id5{did you choose a certificate ?}
  id5-- yes ---id3
  id5-- no ---id6
  id6[[signature process]]
  id7[[insert claims in VP <br/>and VP unsigned]]
  id3-->id7
  id4-->id5
  id6-->id7
  id8[[check certificate used by VP]]
  id9[[insert  certification claims into VP]]
  id8-->id9
  id7-->id9
  id3-->id8
  id10[[check Verifiable Presentation or claims]]
  id9-->id10
  id11[[generate Verifiable Credential signed]]
  id10-->id11
  id12[(Service Verifiable Credential)]
  id11-->id12
```

* If the candidate to the service creation has credentials validated by an authority recognized at EU level (https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home) , he will sign the Verifiable Presentation with its x509 certificate and provide a did:web for that certificate. 
* self declared information should be digitally signed by a Digital Signature Service Prodiver. 

### 5.5.4. Manual verification of a document (claim)

Manual verification if __out of scope__ for November.

### 5.5.5. Receipt of Verifiable Credentials from organizations 

Participants provides Verifiable Credentials including from organization. 
These organizations MUST be in a list from the Trust anchor component. 

These checks are __out of scope__ for november 2022.

#  6. Issuing Verifiable Credentials or Verifiable Presentation

A VC Issuer provides services to sign Verifiable Credentials or Verifiable Presentations with a valid certificate and a valid DID.

## 6.1. VC Issuer

### 6.1.1. Goal 
The purpose of the work package is to deliver Verifiable Credentials that will assert the compliance to standards required by Gaia-x. 
Those verifiable credentials (VCs) will be issued in accordance with the compliance scheme of the compliance references.

In a single input file, for any compliance reference, any service and any location (able to provide required informations, would be provided to generate the VCs.

### 6.1.2. Input Data
This input file identify SHALL identify:
* for the compliance reference
  * an identifier of the compliance reference
    * description of the compliance reference
    * version of the compliance reference
    * validity date of the compliance reference
    * link  targeting the reference file with the compliance reference in a pdf format
    * available third party for compliance assertion
  * an identifier of the compliance reference owner
  * description of the compliance reference owner
* for the assertion method
  * Self assertion method
  * Third-Party assertion method
  * Third Party Identifier
  * for the service:
    * an identifier of the service
    * a description of the service
    * location where the service is available
    * for the location;
      * an identifier of the location
      * a description of the location 

### 6.1.3. Deliverables

For each compliance reference, each service and each location as described in the input data section, VC shall be delivered:
* in JSON-LD format
* compliant with W3C Verifiable Credentials Data Model v1.1 recommendation 
* whose "subject" will be the service identified by a unique identifier in reference to the DID Web.
* whose compliance reference will be identified by a unique identifier with reference to the DID of the authority that issued this compliance reference 
* whose conformity certification will be identified by a unique identifier with reference to the DID of the certification authority, if applicable (in the case of a "self-assessment" with reference either to the DID of the authority in charge of the reference system or the participant DID » 
    * signed by the participant with reference to the participant DID 
    * including, if necessary, location information with reference to participant DID 
The VCs will MUST be available:
* to the public by participants as files in JSON-LD format 
* using the did:web format for managing its DID

## 6.2. Repositorty for a participant

### 6.2.1. Goal
__This part is out of scope for November 2022__

The goal of this batch is to provide a repository for a participant. 

We have retained that the following roles as partners engaged in the compliance framework be handled in the current work package.
* Cloud Service Provider
* Organization managing compliance standards
* Certification body for Third-Party compliance assertion method,
* GAIA-X as manager of a “Trust Framework” 
 
### 6.2.2. Deliverables

For each here above defined roles the system will implement of the following functions:
* registration of a did for the actor in question (verification by RSA 4096 key)
* implementation of a WEB server to access the VCs and the DID
* import of VCs input data in batch mode for generation of VCs (input file in CSV format) 
* selection of VCs to produce and checks necessary for the production of the VCs (only the VCs invoked in the batch file will be checked) 
* production and availability on the VC website for interaction with other actors 

The application delivered will be identical for the 4 roles. The adaptation of the application to the role will be carried out through the initial configuration of the latter. Once deployed the application will not likely change the role it performs. 
The realization of the complete VC system requires the implementation of 4 roles. 

# 7. Service lifecycle management

All Verifiable Credentials MUST have a validity period.
All Verifiable Credentials/Presentations MAY be revoked by the owner and the owner only. 

All Verifiable Credentials MUST manage a [revokation list as defined by w3c](https://w3c-ccg.github.io/vc-status-rl-2020/#:~:text=The%20verifiable%20credential%20that%20contains,that%20includes%20the%20RevocationList2020Credential%20value.&text=The%20type%20of%20the%20credential,revocation%20list%2C%20MUST%20be%20RevocationList2020%20.) 

It's out of scope for November 2022 but it's a nice to have for November 2023. 

# 8. Components View 

Principles for components: 
* MUST provide an API
  * API MUST be a standard each time it's possible 
  * API MUST be a REST API. An other way to provide API MUST be justified.
* MUST provide a way to install and start it
  * Components may be provided as containers 
* MAY be implemented in any language. 
* MAY reuse existing components each time it's possible
  * These components MUST be open source

The Rules-Checker service is integrated in this system:
```mermaid
flowchart TD
    Notorization --> form
    form('Service Description Wizard tools') --> rc('Rules Checker')
    rc --> pr("Policy Rules")
    form --> APICatalog("API Catalog")
    rc --> TrustAnchorRegistry 
    rc --> vci("VC Issuer")
    rc --> sv('Schema Validator')
    style rc fill:#87CEFA
    style vci fill:#87CEFA
    style form fill:#87CEFA
    style Notorization fill:#87CEFA
    style pr fill:#87CEFA
```

_in blue, components in scope of this document_

| Component | Description | 
|---|---|
| Rules Checker API | API request a Gaia-x compliance or a gaia-x label | 
| API Catalog| API to create/update services or search services | 
| Trust Anchor Registry | Registry of trusted certificates / did | 
| Policy Rules | API to execute rules and registry of policy rules |
| VC Issuer | Tool to sign Verifiable Presentations et Verifiable Credentials |  
| Schema validator | Component to check if the input have a valid format using SHACL |  


The Rules checker is organised in 3 components: 
```mermaid
flowchart TD
  rc('Rules Checker') --> sv('Schema Validator')
  rc-->pre('Policy Rules')
  rc --> rcv('Rules Checker Volume')
```

| sub components | Description | 
|---|---|
| Rules Checker | The rule checker check the input using the schema validator then call the Policy Rules component, the rules are valid, then sign a vc and return it and store  it.|
| Policy Rules (OPA) | The Policy Rule have some rules defined in Rego and executed when requested by the Rules checker. This component can not be used by other components |
| Schema Validator | This component perform a SHACL control to check the input is compliant with Gaia-x ontologies |
| Rules Checker Volume | This component store all VCs signed and MAY be called using the Rule Checker API (GET /vc/{id} ) |


A Verifiable Presentation for a Participant or Services are the input for the Rules-checker.  
The output of the Gaia-X Compliance service is a Verifiable Credential 

## 8.1. API

HTTP POST 

| HTTP Verb | url | input | output | Description |
|---|---|---|---|---|
| POST | /rules/{rulesGroupe}/{ruleName}  | VC or VP, see ruleName | VC Compliance |
| GET| /vc/{id} | n/a | a VC compliance or a VC labelling |Retreive the VC signed already provided|
|GET|/vc/{id}/.well-known/did.json | n/a | a VC compliance or a VC labelling|Retreive the VC signed already provided |
|GET|/countVC/|n/a| {"statut":"true" // or false and  ,"count":273 } | Get the count of VC already signed|

The rules url have theses parameters: 
| ruleGroup | ruleName | Input | Output |
| ------ | ------ | ------ | ------ |
| compliance | complianceParticipant | a VC Participant | a Verifiable Credential signed for the compliance to Gaia-x |
| compliance | complianceServiceOffering | a VC locatedServiceOffering | a Verifiable Credential signed for the compliance to Gaia-x|
| labelling | label | a Verifiable Presentation with at least a VC locatedServiceOffering and all requerid VC | a Verifiable Credential signed for the Gaia-x Label provided|

This service will call Policy Rule server (OPA) with those values, it's allowed to add rules and call them with this service without updating this service. 
This call return a VC signed.

## 8.2. Formats 

### 8.2.1. Format Verifiable Credential Participant (Provider)

```javascript
{
    "@context": [
      "https://www.w3.org/2018/credentials/#",
      {
        "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "vcard": "http://www.w3.org/2006/vcard/ns#",
        "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
      }
    ],
    "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/provider.json",
    "@type": [
      "verifiableCredential"
    ],
    "issuer": "did:web:abc-federation.gaia-x.community",
    "issuanceDate": "2022-09-23T23:23:23.235Z",
    "credentialSubject": {
      "@type": [
        "gax-participant:Provider"
      ],
      "gax-participant:hasLocations": [
        {
          "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/location/09.json",
          "@type": "gax-participant:Location"
        },
        {
          "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/location/12.json",
          "@type": "gax-participant:Location"
        },
        {
          "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/location/13.json",
          "@type": "gax-participant:Location"
        }
      ],
      "gax-participant:hasDidWeb": {
        "@value": "did:web:ovh.provider.gaia-x.community",
        "@type": "xsd:string"
      },
      "gax-participant:hasServiceOffering": [
        {
          "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/service-offering/S-01.json",
          "@type": "gax-service:ServiceOffering"
        },
        {
          "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/service-offering/S-02.json",
          "@type": "gax-service:ServiceOffering"
        }
      ],
      "gax-participant:registrationNumber": {
        "@value": "424 761 419 00045",
        "@type": "xsd:string"
      },
      "gax-participant:legalAddress": {
        "@type": "vcard:Address",
        "vcard:country-name": {
          "@value": "FR",
          "@type": "xsd:string"
        },
        "vcard:street-address": {
          "@value": "2 Rue Kellermann",
          "@type": "xsd:string"
        },
        "vcard:postal-code": {
          "@value": "59100",
          "@type": "xsd:string"
        },
        "vcard:locality": {
          "@value": "Roubaix",
          "@type": "xsd:string"
        }
      },
      "gax-participant:headquarterAddress": {
        "@type": "vcard:Address",
        "vcard:country-name": {
          "@value": "FR",
          "@type": "xsd:string"
        },
        "vcard:street-address": {
          "@value": "2 Rue Kellermann",
          "@type": "xsd:string"
        },
        "vcard:postal-code": {
          "@value": "59100",
          "@type": "xsd:string"
        },
        "vcard:locality": {
          "@value": "Roubaix",
          "@type": "xsd:string"
        }
      },
      "gax-participant:leiCode": {
        "@value": "969500Q2MA9VBQ8BG884",
        "@type": "xsd:string"
      },
      "gax-participant:termsAndConditions": {
        "@type": "gax-core:TermsAndConditions",
        "gax-core:Value": {
          "@value": "https://www.ovhcloud.com/fr/terms-and-conditions/",
          "@type": "xsd:anyURI"
        },
        "gax-core:hash": {
          "@value": "31296ef8727fc63e89213f3e5ab928bae7699b6dade0af5e443b8ad2623639ee8e6176696a1fa125ccfa7fa55465463b6424d3f54435d6a2beafd79a91425b0a",
          "@type": "xsd:string"
        }
      }
    },
    "proof": {
      "type": "JsonWebSignature2020",
      "proofPurpose": "assertionMethod",
      "verificationMethod": "did:web:abc-federation.gaia-x.community",
      "created": "2022-10-20T10:41:00.541875+00:00",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..ccBC79pc52G25cXHB-QnWeTfTYthOuEDcnRBxx1tivnIqPj5-QvX3vEOp_iR6H1MS7yBSZOhwPH1loJcXMsrSuiJVhamwMnGlE_vDPQ6hjCc6dqe8xC6g9SG7wdb5Lv5H5dv4k-4GgzOMj5ebtudX9ng1VS_tLiPw9_D6yBwWAD4YVroOor41GAxntIGH1_Wc8oN__aog_eGue5ZrnHFHdu12qzHDhzqi-5AWN0qEELOnsJWU6SVz1JNfyVobzdznCQQemvmzB5TaKjIK-E2Q9AHAI7bi6eVy-PLEIyuDSC3qK6e47kill9myN55znanyXSt0OAjlI5RKrak4-wksg"
    }
  }  
```

### 8.2.2. Format Verifiable Credential Participant 

The participant VC has to be compliante with Gaia-x ontology.
```javascript
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1"
  ],
  "type": [
    "VerifiableCredential",
    "LegalPerson"
  ],
  "id": "https://abc-federation.gaia-x.community/public-data/participant.json",
  "issuer": "did:web:abc-federation.gaia-x.community",
  "issuanceDate": "2022-09-23T23:23:23.235Z",
  "credentialSubject": {
    "id": "did:web:abc-federation.gaia-x.community/public-data/participant.json",
    "type": "gx-participant:Participant",
    "gx-participant:name": "Fictive federation using Gaia-X federation services",
    "gx-participant:legalName": "ABC federation",
    "gax-participant:registrationNumber": {
      "@value": "493376008",
      "@type": "xsd:string"
    },
    "gax-participant:legalAddress": {
      "@type": "vcard:Address",
      "vcard:country-name": {
        "@value": "FR",
        "@type": "xsd:string"
      },
      "vcard:street-address": {
        "@value": "2 Rue Kellermann",
        "@type": "xsd:string"
      },
      "vcard:postal-code": {
        "@value": "59100",
        "@type": "xsd:string"
      },
      "vcard:locality": {
        "@value": "Roubaix",
        "@type": "xsd:string"
      }
    },
    "gax-participant:headquarterAddress": {
      "@type": "vcard:Address",
      "vcard:country-name": {
        "@value": "FR",
        "@type": "xsd:string"
      },
      "vcard:street-address": {
        "@value": "2 Rue Kellermann",
        "@type": "xsd:string"
      },
      "vcard:postal-code": {
        "@value": "59100",
        "@type": "xsd:string"
      },
      "vcard:locality": {
        "@value": "Roubaix",
        "@type": "xsd:string"
      }
    },
    "gax-participant:leiCode": {
      "@value": "969500Q2MA9VBQ8BG884",
      "@type": "xsd:string"
    },
    "gx-participant:termsAndConditions": "70c1d713215f95191a11d38fe2341faed27d19e083917bc8732ca4fea4976700"
  },
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:abc-federation.gaia-x.community",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..G58eH2B0k7vCNUqbKkDmLX6ySd60l7lzaTR9IofY7RK1G_kWBdM_pGDjQiXdu7bxx_hU7NwmygHqQkcrnPbwtWwIMjZlkDit6NnmdePlYghGl9TBrJvxyXvWtgkcl6dJdUPrk0GY0sCnMvTf-O-hPV4Rd8qGh_a_qY7LJ4CAqnT26nvg-htdaMbiBub5grYj0kWHN4tQ0mBeWZgLHPMgqBnVEfuhmt-JwcEf6sBj7QtwYVQbXGHxWfzon1xXiGVWccvSPc7Dhf49KestMrOZfimCeiKWaK26R316JxZNFyrrviMDopQJDjpgmNsxR1Az1gKXQ1mO_C_gmmGgxoCGCg"
  }
}
```

### 8.2.3. Format vc Compliance 

The compliance output for participant is a Verifiable credential  which is a common object for all the compliance output.
```javascript
{    
  "@context": [
        "https://www.w3.org/2018/credentials/v1" ,
        {
          "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        }
  ],
  "@id": "https://ruleschecker.abc-federation.gaia-x.community/vc/xxx",
  "@type": "VerifiableCredential",
  "credentialSubject": {
    "vc": "did:web:ovhcloud.provider.gaia-x.community:participant:xxx/provider/vc/xxx/data.json",
    "@type" : "gax-compliance:ComplianceObject",
    "targetType" : "Participant",
    "vcProof": {
      "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2022-10-26T15:29:32.882456+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..taNLLHtr5IWgTgTBuJS1eS7uiZHs5KF_ogEjPVi7JXKaynKxrMKvg8_me7ecaWdRIgWKKfd4SY0caHta3bGlc4-ZoLFgAqKmCnom9zh9rFEOdPdhumDNOCyMAy6yjn37-4goaiTls2kz3NWGON0yH-g3kbd5mg6DmEV8JtAkA13QvBKBSjmwDdEwoAihqDxOsa-Dpqwe3N_DswBzvprcWzAARZVlOm8lkrMpBpqD9OvngaJxChGj1IQFnXT0LWwai-ENsCzkl9bsxAkFbWazJpRulBp_-Fz5YBjnNbXG1RqwrBU3ws5sxGSYNrwO7TcYHKqOEKfB2yuo3GOGI1GbmA"
    },
    "validRules": [
      "CPR01_checkJsonld",
      "CPR02_checkSchema",
      "CPR03_checkSignature",
      "CPR04_checkRegNum",
      "CPR05_checkUSState",
      "CPR06_checkLeiCodeInHQuarter",
      "CPR07_checkLeiCodeInLegAddress",
      "CPR09_checkDid"
    ]
  },
  "evidence": {
    "documentPresent": "digital",
    "subjectPresent": "digital",
    "type": [],
    "verifier": ""
  },
  "expirationDate": "2022-10-26T15:29:32.872Z",
  "issuer": "did:web:abc-federation.gaia-x.community2",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:abc-federation.gaia-x.community",
    "created": "2022-10-26T15:29:32.882456+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..taNLLHtr5IWgTgTBuJS1eS7uiZHs5KF_ogEjPVi7JXKaynKxrMKvg8_me7ecaWdRIgWKKfd4SY0caHta3bGlc4-ZoLFgAqKmCnom9zh9rFEOdPdhumDNOCyMAy6yjn37-4goaiTls2kz3NWGON0yH-g3kbd5mg6DmEV8JtAkA13QvBKBSjmwDdEwoAihqDxOsa-Dpqwe3N_DswBzvprcWzAARZVlOm8lkrMpBpqD9OvngaJxChGj1IQFnXT0LWwai-ENsCzkl9bsxAkFbWazJpRulBp_-Fz5YBjnNbXG1RqwrBU3ws5sxGSYNrwO7TcYHKqOEKfB2yuo3GOGI1GbmA"
  }
}
```

Elements of "gax-compliance:ComplianceObject":
* "vc"
  * Id of the VC used as input
* "vcProof"
  * Target proof
  * Used to ensure that the core of the targeted VC didn't change or expire
* "targetType"
  * Input VC type 
  * Allows to differentiate between participant or located service offering
* "validRules"
  * List of rules used by the compliance to validate the verifiable credential

### 8.2.4. Format Verifiable Presentation Service 

This Verifiable Presentation is a Service Offering as defined in Gaia-x Ontology. 
This verifiable Presentation MUST include some claims to get a label like X509 or pdf. 
```javascript 
{
   "@context":[
      "https://www.w3.org/2018/credentials/#"
   ],
   "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/4429512748ebd2924cf50e2b3efee377e76038e346a4d3ad266d47dfcf1aa5df/data.json",
   "@type":[
      "VerifiablePresentation"
   ],
   "verifiableCredential":[
      {
         "@context":[
            "https://www.w3.org/2018/credentials/#"
         ],
         "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/5d61abb4ff29f50abbf4d21e0c3696dd5b2693fe3deaae3275f2d500ecc8b5c1/data.json",
         "issuer":"did:web:dufourstorage.provider.gaia-x.community",
         "@type":[
               "VerifiableCredential"
         ],
         "credentialSubject":{
            "@context": {
               "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
            },
            "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/located-service-offering/9e9caae3a13c28cad5371c3703705688191d0a082e33ef87d8a79fff9acb8760/data.json",
            "@type":"gax-service:LocatedServiceOffering",
            "gax-service:isImplementationOf":{
               "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/service-offering/16ea3d9c5c9ef155dfee355366b44fc7119afdbeb84b809d54518b253d67310d/data.json",
               "@type": "gax-service:ServiceOffering"
            },
            "gax-service:isHostedOn":{
               "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/location/f17220a5c93bdc04451b1253015b4e2c2dc1cd532b9e4e6e75f13f39080024f6/data.json",
               "@type": "gax-service:Location"
            },
            "gax-compliance:hasComplianceCertificateClaim":[
               {
                  "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/3510267eaf591f87b3d217a4348a2d26d374b6bfb9b82e2d4764192196cb6b6b/data.json",
                  "@type":"gax-compliance:ComplianceCertificateClaim"
               },
               {
                  "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/510267eaf591f87b3d217a4348a2d26d374b6bfb9b82e2d4764192196cb6b6b3/data.json",
                  "@type":"gax-compliance:ComplianceCertificateClaim"
               }
            ]
         }
      }
   ],
   "proof":{
      "type":"JsonWebSignature2020",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
      "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..LQZbnJQq-Jh75N33D8MXFgiFPvvw1EFK5FuUXbsOgQfy9U_EEGxQ9D2LgnWIhTRWOBQl3IYvanH-2ggGlg3YhQlQpCedgc1XWci-E_a5tjZAyjg-oKqN265yqjw3xbMvfHUfcHyR-F4FgYANIxQNixHvv9Kuj-6sjAANi-KwLZnyDVrTxZ0jBS4VJoIXuGeHBETaE7mciiinBZjvbRjOv76MHEQJnzf4YcOngcKQtaSGYB5dXc4hxe7fEpe4q3kSeO2xmuCwwSMjpgijoTnIrYPMej9i3lnkjUusWIbkbQch4Bk2C9MCsxGR-wDUIey5VbVVP9eru3hpiEO4Qi2eig"
   }
}
```

### 8.2.5. SecNumcloud Claim 

SecNumCloud have to be specified by ANSII. 
The Notarization will use this format: 

```javascript 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    {"secnumcloud" : "https://schemavalidator.abc-federation.gaia-x.community/ttl/secnumcloud.jsonld"}
  ],
  "id": "http://auditor.gov/secnumcloud/cloudprovider.eu",
  "type": ["VerifiableCredential", "SecNumCloud"],
  "issuer": "did:web:auditor.gov",
  "issuanceDate": "2010-01-01T19:23:24Z",
  "expirationDate": "2030-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "secnumcloud:level" : "1"
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2017-06-18T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.edu/issuers/565049#key-1",
    "jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X"
  }
}
```

### 8.2.6. X509 Claim 

Certificates MUST be used to sign the Verifiable Presentation with a did:web providing information about keys like this one : 

```javascript 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "http://example.edu/credentials/1872",
  "type": ["VerifiableCredential", "x509"],
  "issuer": "https://example.edu/issuers/565049",
  "issuanceDate": "2010-01-01T19:23:24Z",
  "expirationDate": "2030-01-01T19:23:24Z",
  "credentialSubject": {
    "kty": "RSA",
          "n": "oiV5WxxfuZGVm02szPbvEaiWMr9EwLrwXytoM65PaVdiCqxe9xSsgvtdqeCjyaxNub2TECDHOiMdhg0gs7ww8KGrnw54g8gyIO48UIh5t0IQTmzS9URdW5wU7-UbZOIErDGiy7rcgQ_tZcop1y57cOEgDpHniNJcX97nEpGGZu0tLl57hsqtxrMuWlR7xr5E7Ni9Mf2Dm_Ya9l2kUFygYaQPwy_Fmo5quO3P9Hvf7pk0qMC0hv2KtZ4ObkIIDfKDxdKVDJ-9zO8NYtl861-lH9CD_JFbJc3rqj8gBUlXa-XDgZXOibqg31YpFGK4pokDCFIx8xFYUpkwkLXDq9qtJ_FtHRNeR7zQ71nFGpocaeCp5qq3jVjNrx9kjcPJFDRGbKEuKNbvebGhOjnWwkTeDlgFlsZZalUQuhTea5crDjX-8vEGxMt3kg9BvcQZHWqIuoPfDRCvpvRfuI62G8Sw9QI2TD9yKOxv4qPpa0cnPo6d_Neldul9xAk2-7Eq7ehiT8ok5hG7ExB5aTCeaCk9X9vTBX09caw4ELdoPV5joLdPC1zJP_wS1JCZEnezpZ4sE3vzZ8LqJuGUb6q7US8kMgc3xxVIOoCwTvNkOnG5fFCL7bY23vCf29hFLSnREq0hpb4UZg7irC0YafH8hrs4idS3hYL4Z1-dIev3t0hHOEc",
          "e": "AQAB",
          "alg": "PS256",
          "x5u": "https://did:web:cloudprovider.eu/.well-known/0002_chain.pem"
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2017-06-18T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.edu/issuers/565049#key-1",
    "jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X"
  }
}
```

This did:web MUST include a x5u attribute to get the full chain.
This did allow the compliance service to check some attributes in the Verifiable Presentation. 
The compliance service MUST check the certification chain and check if it's an eIDAS certificate or not with this chain. 
Fields like x5u are defined in this [w3c specification for JWT](https://datatracker.ietf.org/doc/html/rfc7515)

### 8.2.7. ISO 27001 

```javascript 
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    { "iso27001": "https://schemavalidator.abc-federation.gaia-x.community/ttl/iso27001.jsonld"}
  ],
  "id": "http://notarization.a-b-c-federation.gaia-x.community/credentials/1872",
  "type": ["VerifiableCredential", "ISO27001"],
  "issuer": "did:web:a-b-c-federation.gaia-x.c.community",
  "expirationDate": "2030-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:web:did.cloudprovider.eu",
    "iso27001:registrationNumber" : "12356894100056",
    "iso27001:hashISO27001" : "mI2NCI6ZmFsc2UsImNyaXQiOls",
    "iso27001:urlPdfISO27001":"https://www.cloudprovider.eu/document/mI2NCI6ZmFsc2UsImNyaXQiOls"
  },
  "proof": {
    "type": "RsaSignature2018",
    "created": "2017-06-18T21:19:10Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "https://example.edu/issuers/565049#key-1",
    "jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5X"
  }
}
```

### 8.2.8. Format VC Terms and Conditions 

The VC Terms and Conditions is defined in these specifications : https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/

```javascript 
{
   "@context": [
    "https://www.w3.org/2018/credentials/v1",
    { "tac": "https://schemavalidator.abc-federation.gaia-x.community/ttl/tac.jsonld"}
  ],
  "id": "did:example:issuer/tandc",
  "type": [
    "VerifiableCredential"
  ],
  "issuer": "did:web:did.cloudprovider.eu",
  "issuanceDate": "2022-06-12T19:38:26.853Z",
  "credentialSubject": [
    {
      "@id": "did:example:issuer/tandc/1",
      "type": "GaiaXTermsAndCondition",
      "tac:termsAndConditions": {
        "tac:hash": "0f5ced733003d11798006639a5200db78206e43c85aa123456789789123456798",
        "tac:checksumtype": "SHA-256"
      }
    }
  ],
  "proof": {
    "type": "JsonWebSignature2020",
    "verificationMethod": "did:example:issuer#key",
    "created": "2022-06-12T19:38:26.853Z",
    "proofPurpose": "assertionMethod",
    "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S"
  }
}
```
