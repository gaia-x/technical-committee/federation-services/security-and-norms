# Table of Contents
1. [References](#references)
2. [Glossary](#glossary)
3. [Introduction](#introduction)
4. [Cloud Service Provider subscribe form](#cloud-service-provider-subscribe-form)
5. [Claims](#claims)
6. [Labelling](#labelling)
6. [Components View](#components-view)

# References

| Code | Link |
| ------ | ------ |
| W3C Verifiable Credential | https://www.w3.org/TR/vc-data-model/  |
| Gaia-x DID Auth | https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/blob/14-did-sig-proposal-for-a-full-decentralized-authentication-authorization-mode/docs/did-sig.md |
| W3C VP request | https://w3c-ccg.github.io/vp-request-spec/ |
| Gaia-x Architecture| https://gaia-x.eu/wp-content/uploads/2022/06/Gaia-x-Architecture-Document-22.04-Release.pdf | 
| Gaia-x Trust Framework | https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/ | 
| Gaia-X Labelling Framework | https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf |
| Gaia-X Labelling Criteria | https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X-labelling-criteria-v22.04_Final.pdf |
| Gaia-X Policy Rules Document | https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X_Policy-Rules_Document_v22.04_Final.pdf |
| Gaia-x Ontology | https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/core/core.html |

# Glossary

| Term | Definition |
| ------ | ------ |
| Digital Trusted Service | A trust service provider (TSP) is a person or legal entity providing and preserving digital certificates to create and validate electronic signatures and to authenticate their signatories as well as websites in general. Trust service providers are qualified certificate authorities required in the European Union and in Switzerland in the context of regulated electronic signing procedures. [source Wikipedia](https://en.wikipedia.org/wiki/Trust_service_provider) |
| Digital Trust Services Portal | Service (Web Page) displaying the list of trusted services |
| User | Any user like End User or Service Provider |
| Issuer | an Issuer compliant with Gaia-x requirements |
| Bridge | Component that simplifies existing services with the Gaia-x ecosystem |
| Verifiable Presentation (VP) | A set of one or more claims made by an issuer. A verifiable credential is a tamper-evident credential that has authorship that can be cryptographically verified. Verifiable credentials can be used to build verifiable presentations, which can also be cryptographically verified. The claims in a credential can be about different subjects. (Source W3C) | 
| Verifiable Credentials (VC)| A standard data model and representation format for cryptographically-verifiable digital credentials as defined by the W3C Verifiable Credentials specification [VC-DATA-MODEL]. |
| DID document | A set of data describing the DID subject, including mechanisms, such as cryptographic public keys, that the DID subject or a DID delegate can use to authenticate itself and prove its association with the DID. A DID document might have one or more different representations. |
| CSP | Cloud Service Provider |
| Candidate | A CSP is considered as a candidate if he is subscribing to the CSP suscribe page but has not completed the subscription process (completion of the form, verification of his declarations) and is therefore not yet registered. |
| User | A CSP is considered as a user if he has completed the CSP subscription process and is therefore registered. |
| Auditor | A user checking document and with the ability to validate the input. An auditor may be an external company |
| Service Provider | Gaia-x Participant publishing a Service Offering | 
| Consumer | Gaia-x participant using one or more Gaia-x Services |

# Introduction 
__(@Docaposte)__

![image](Assets/ComplianceProcess.png)

A Service Provider must start by providing a Verifiable Presentation for a service to publish it ([like this sample](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/main/src/tests/provider-instance-test.json))
The Service Provider MUST upload required documents for the label. 
Documents MAY be : 
* a certification as pdf (PDF/a) 
* a certification as Verifiable Presentation (from a trusted auditor or issuer)
* use a valid eIDAS Certificate for the identity (TODO define process, challenge to sign with this certificate ?)

The Compliance Service perform technical checks as defined in the [trust framework](http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors)
* syntactic correctness.
* schema validity.
* cryptographic signature validation.
* attribute value consistency.
* attribute value verification.

The compliance service provide ( [source Trust Anchors|(http://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors) ):
* as a Verifiable Credential: If the compliance is validated, the Gaia-X Compliance Service issues a Verifiable Credential that can later be inserted into the Self-Description. This method aligns with the self sovereign principle of the holder being in charge of the information.
* If the Gaia-X Compliance Service is called via the Gaia-X Registry, the Gaia-X Registry will emit an event to synchronize Catalogues. The event contains the URL the of the Self-Description file. The Gaia-X Registry is defined in the next section.

Some links to Participants to some [Digital Trust Services](./DigitalTrustServices.md) to prove Participant's identity : 
  * services to get a valid certificate
  * sign a document 

# Cloud Service Provider subscribe form 
__@Atos__

## Portail & API to create a service 

Data from IAM  JWT (link with lot  1.1)
* User accessing the enrollment service can be previously authenticated thanks to an Identity Provider 
* This will be materialized by the provisioning of a JWT (ref RFC7519) token containing claims 
* The token is provided in the Authorization header of the HTTP request with format “bearer<space><token>”
* confidentiality and authenticity are offered by HTTPS (mandatory - TLS1.3 recommended)
* The JWT token can be provided by 
  * An IAM known as Gaia-X trust provider : here we can think about the use case where a company already providing a Gaia-X service wants to enroll a new one
  * An IAM considered trusted by the enrollment service itself, it can be a national IDP like France connect or any other listed in the service context with access to public key to check the validity of the token.
  * Any IAM; the claims contained in the JWT token can be proposed as pre-filled values in the registration form The registration form itself is composed of an HTML form listing all inputs needed for the enrollment and a REST api service used by the form and who can be used by a program

The input expected can be in the form of 
* Text 
  * Name of the company
  *	Name of the service 
* Files 
* certificate (like ISO)
* Links to online certificates
  * X509 Certificate
  * Verifiable Credentials (or link to a VC)
* Content definition (technical and documentary) of the subscribe form and mockup (certifications list etc. from the PRD)  ( + API for a CSP subscription ?)
Claims : a service can depend on another (label inheritance)
* Requests/submission of supporting documents : Description and mockup (+ upload of VC links)
* The service shall provide interfaces to cancel the current subscription (no action at all afterward) or to delete the subscription. 
* The update of the subscription is possible (pending state) to allow the candidate to draft its form
  * (A valider) the verification services (§2.2) could be called dynamically and the form can display immediately a validation status if available.
* Once the form submitted, the verification process can occur, the candidate can display the form and a component will inform him regarding the status
  * In following use cases, validation can fail 
    * Expiration or revocation of an x509 certificate
    *	... (see details in §5)

![image](Assets/portal.png)

## Claims

upload claims 

## Supporting documents required 
__@IMT (@Outscale @Atos @Docaposte @OVH)__

* Required documents table (linked with the PRD document) by certification
* Document upload, Link upload (VC) (+API)

COMPLETE LE July 6th

## Supporting document validation

Supported organizations list (ability to VC emission?) 

### Supporting document validation in the current context
__ owner ? __

#### Automated exchange with official certification lists 
__@Outscale__

* Certification validation systems list table
* Exchange format with certification validation systems 
* Exchanged flows ( Inputs/Outputs, etc.) 
* Documentary analysis for extracting relevant information, example : analysis of AFNOR certificate to extract the company, date, type of certificate
@Bastien  : How they control their clients certification    

List defined in the [Table defined in Gaia-x Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/trust_anchors/)
List of defined trust anchors :
* State	The Trust Service Providers (TSP) must be a state validated identity issuer.
  - For participant, if the legalAddress.country is in EEA, the TSP must be eiDAS compliant.
  - Until the end of 2022-Q1, to ease the onboarding and adoption this framework, DV SSL can also be used.
  - Gaia-X Association is also a valid TSP for Gaia-X Association members.
* eiDAS	Issuers of Qualified Certificate for Electronic Signature as defined in eIDAS Regulation (EU) No 910/2014
  (homepage: https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home)
  (machine: https://ec.europa.eu/tools/lotl/eu-lotl.xml)
* DV SSL	Domain Validated (DV) Secure Sockets Layer (SSL) certificate issuers are considered to be temporarily valid Trust Service Providers.
(homepage: https://wiki.mozilla.org/CA/Included_Certificates)
(machine: https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV)
* Gaia-X	To be defined after 2022Q1.
* EDPB CoC	List of Monitoring Bodies accreditated to the Code of Conduct approved by the EDBP
(list of EDBP’s CoC: https://edpb.europa.eu/our-work-tools/documents/our-documents_fr?f%5B0%5D=all_publication_type%3A61&f%5B1%5D=all_topics%3A125)
* gleif	List of registered LEI issuers.
(homepage: https://www.gleif.org/en/about-lei/get-an-lei-find-lei-issuing-organizations)
(machine: https://api.gleif.org/api/v1/registration-authorities)

For Nov VC are out of scope 

#### Link to automated supporting document analysis services
__@Docaposte @IMT__

Automated Document Analysis is __Out of scope__ for november 2022. 
In some future release above November 2022:
* Supporting document analysis service directory by document type
* Content description and mockup of the service directory page by document type 
* Supporting document  analysis service description and subscribe standard pages (+ link to the API catalog by service)

#### Connector to list of trusted validation systems
__@Atos, @Docaposte__

```mermaid
flowchart TB
  id1((start))
  id2{certificate from <br/>an authority recognized by EU ?}
  id1-->id2
  id3[[Sign a VP with it]]
  id2-- yes ---id3
  id4[[Use a Digital Trust Service ]]
  id2-- no ---id4
  id5{did you choose a certificate ?}
  id5-- yes ---id3
  id5-- no ---id6
  id6[[signature process]]
  id7[[upload identity claims <br/>and VP unsigned]]
  id3-->id7
  id4-->id5
  id6-- no upload claim ---id7
  id8[[check certificate used by VP]]
  id9[[upload certification claims]]
  id8-->id9
  id7-->id9
  id3-->id8
  id10[[check Verifiable Presentation or claims]]
  id9-->id10
  id11[[generate Verifiable Credential signed]]
  id10-->id11
  id12[(Service Verifiable Credential)]
  id11-->id12
```

* Content description and mockup of trusted validation system directory by domain
* If the candidate to the service creation has credentials validated by an authority recognized at EU level (https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home) , he will provide its x509 certificate which will be verified 
* self declared information should be digitally signed in a format to be defined 
* If the candidate has no certificate from a trusted provider, the subscribe service proposes a directory of trusted services and APIs to subscribe. 
* The candidate can reduce list of service providers thanks to a filter feature : by country code, by service type
* Once service selected, a standard page of description is displayed
* Once digitally signed information provided, the candidate can trigger its validation through a trusted validation system. this system is described in details in a dedicated page : the portal requires a verification using an API and displays the complete verification report. (to ensure the transparency of the process, the candidate can verify himself the validity of the signature using the information provided)
* CSP’s EIDAS certificate verification

#### Manual verification 

Manual verification if __out of scope__ for November.

#### Receipt of Verifiable Credentials from organizations 
__@Outscale__
 
# Labelling 

## Generation of a GAIA-X label based on data from the subscribe form
__@IMT (@Outscale @Atos @Docaposte @OVH )__

* Labeling algorithm based on the Policy Rules Documents. How a CSP could inherit GAIA-X label from another ?
* Algorithm maintenance : use a ruleset old version ?

# Generation of verifiable credentials
__@OVH__

[comment ymp]: this document is not an OVH Spec, thanks to replace OVH by Service Provider  )

(Links with lots 1.1 and 1.3)
## Work Package 1.5.4.1

The purpose of the work package is to deliver verifiable credentials that will assert the compliance to compliance references for OVHCloud Services in every Location where those services are implemented.
Those verifiable credentials (VCs) will be issued in accordance with the compliance scheme of the compliance references.

In a single input file, for any compliance reference, any OVHCloud Services and any location, would be provided to generate the VCs 
This input file identify shall identify:
* for the compliance reference
  * an identifier of the compliance reference
    * description of the compliance reference
    * version of the compliance reference
    * validity date of the compliance reference
    * link  targeting the reference file with the compliance reference in a pdf format
    * available third party for compliance assertion
  * an identifier of the compliance reference owner
  * description of the compliance reference owner
* for the assertion method
  * Self assertion method
  * Third-Party assertion method
  * Third Party Identifier
  * for the service:
    * an identifier of the service
    * a description of the service
    * location where the service is available
    * for the location;
      * an identifier of the location
      * a description of the location 

## Deliverables

For each compliance reference, each service and each location as described in the input data section, VC shall be delivered; 
* in JSON-LD format
* compliant with W3C Verifiable Credentials Data Model v1.1 recommendation 
* whose "subject" will be the service identified by a unique identifier in reference to the DID of OVHVloud 
* whose compliance reference will be identified by a unique identifier with reference to the DID of the authority that issued this compliance reference 
* whose conformity certification will be identified by a unique identifier with reference to the DID of the certification authority, if applicable (in the case of a "self-assessment" with reference either to the DID of the authority in charge of the reference system or to the OVHCloud DID » 
    * signed by OVHCloud with reference to the OVHCloud DID 
    * including, if necessary, location information with reference to the OVH Cloud DID 
The VCs will be made available to the public by OVHCloud as files in JSON-LD format. 
These files will be the property of OVHCloud. Those file could be made available for testing in the form of a WEB server using the did:web format for managing its DID. 
 
## Work Package 1.5.4.2
Purpose
Many element of this work package are still to be defined.
We have retained that the following roles as partners engaged in the compliance framework be handled in the current work package.
·	OVHCloud as Cloud Service Provider
·	Organization managing compliance standards
·	Certification body for Third-Party compliance assertion method,
·	GAIA-X as manager of a “Trust Framework” 
 
## Input Data
 A complete ontology for each of the objects handled by the 4 roles would be provided as input data. It assumes that the Organizations managing compliance standards and the certification bodies will keep a global certification ontology with reference to a document.
 
The description of services (self-description), locations (geographical area) and labels will be likely to be composed. 
 
The ontologies necessary for the provision of the VC system for the implementation of the VC system are provided by OVHCloud and will mainly result from this work within the framework of work package 1.1 and work package 1.3 in which the "self-description" of service are planned. 
 
## Deliverables
For each here above defined roles the system will implement of the following functions:
* registration of a did for the actor in question (verification by RSA 4096 key)
* implementation of a WEB server to access the VCs and the DID (Apache server under Linux) 
* import of VCs input data in batch mode for generation of CVs (input file in CSV format) 
* selection of VCs to produce and checks necessary for the production of the VCs (only the VCs invoked in the batch file will be checked) 
* production and availability on the VC website for interaction with other actors 

<br/>[comment ymp]: it's a specification, so implementation details like Apache  )
<br/>[comment ymp]: it's a specification, so implementation details like CSV - and a CSV is not the simplier container for json)
<br/>[comment ymp]: why bach mode rather than a service mode and if required asynchronous mode ? )

The VCs system will be developed in python 3.7+, and will provide the necessary python scripts to perform the system functions. A rudimentary WEB interface will be delivered to perform functions using the Flask Framework. 
The application delivered will be identical for the 4 roles. The adaptation of the application to the role will be carried out through the initial configuration of the latter. Once deployed the application will not likely change the role it performs. 
The realization of the complete VC system requires the implementation of 4 roles. The integration of the complete VC System will be handled in work package 1.3 with support of work package 1.5.4.

[comment ymp]: it's a specification, so implementation details like Python, Flask  )

# Service lifecycle management
((( Rappel de commentaire faits au §1 qui se rapportent plutot au 5
 > identifier les cas ou on aurait besoin d’update (pb d’authent’, l’authent’ renvoie une erreur, expiration ... ) 
)))
## Update 
## Delete

# Components View 

## Schema 
```mermaid
flowchart TD
    DistributedAuthentication --> WebCatalog
    WebCatalog --> DistributedAuthentication
    WebCatalog --> WebCompliance
    WebCompliance --> APICompliance
    WebCompliance --> adtp("Digital Trust Service")
    APICompliance --> APICatalog
    adtp --> APICatalog
    portal("Portal") --> DistributedAuthentication
    CheckCertificate --> APICompliance
    APICompliance --> CheckCertificate 
    CheckCertificate --> TrustAnchorRegistry 
    FederationCatalog --> WebCompliance
    style WebCompliance fill:#87CEFA
    style APICompliance fill:#87CEFA
    style CheckCertificate fill:#87CEFA
    style CheckCertificate fill:#87CEFA
    style TrustAnchorRegistry fill:#87CEFA
```
_in blue, components in scope of this document_


The API Compliance MUST provide a REST API to sign a Verifiable Credential with in input ([source](https://gitlab.com/gaia-x/lab/compliance/gx-compliance)): 
```javascript 
{
    "@context": "https://www.w3.org/2018/credentials/v1",
    "type": "VerifiablePresentation",
    "holder": "did:web:www.cloudprovider.eu/vp_service_50",
    "verifiableCredential": [
      {
        "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
        "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
        "@type": ["VerifiableCredential", "LegalPerson"],
        "credentialSubject": {
          "id": "did:web:did.cloudprovider.com",
          "gx-participant:registrationNumber": {
            "@type": "xsd:string",
            "@value": "DEANY1234NUMBER"
          },
          "gx-participant:headquarterAddress": {
            "@type": "gx-participant:Address",
            "gx-participant:country": {
              "@type": "xsd:string",
              "@value": "DEU"
            }
          },
          "gx-participant:legalAddress": {
            "@type": "gx-participant:Address",
            "gx-participant:country": {
              "@type": "xsd:string",
              "@value": "DEU"
            }
          }
        },
        "proof": {
          "type": "JsonWebKey2020",
          "created": "2022-06-17T07:44:28.488Z",
          "proofPurpose": "assertionMethod",
          "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..t_UEs8yG-XXXXXXXXXXX",
          "verificationMethod": "did:web:vc.transmute.world#z6MksHh7qHWvybLg5QTPPdG2DgEjjduBDArV9EF9mRiRzMBN"
        }
      },
      { 
        "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
        "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
        "@type": ["VerifiableCredential", "CertificateeIDAS"],
        "credentialSubject": {
          "owner" : "did:web:did.cloudprovider.eu" , 
          "publicKey" : "GDDSGGGD4566436"
        },
          "proof": {
             "type": "x509",
             "created": "2021-11-13T18:19:39Z",
             "verificationMethod": "https://example.edu/issuers/14#key-1",
             "proofPurpose": "assertionMethod",
              "proofValue": "z58DAdFfa9SkqZMVPxAQpic7ndSayn1PzZs6ZjWp1CktyGesjuTSwRdoWhAfGFCF5bppETSTojQCrfFPP2oumHKtz"
          }
      }
 ],
    "proof": {
      "type": "RsaSignature2018",
      "created": "2020-03-27T21:04:27Z",
      "verificationMethod": "did:web:vc.transmute.world#z6MksHh7qHWvybLg5QTPPdG2DgEjjduBDArV9EF9mRiRzMBN",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19.jYAsUftMzjTw6Dt9DZnM5wbiPzLyGMrd4xYZ1f74bmoInDGP9QufL7gGCTJdIiPCtj26rX_H6Sa-gAcRfdQOBw"
    }
  }
```

and the output : 
```javascript
{
  "complianceCredential": {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    "@type": ["VerifiableCredential", "ParticipantCredential"],
    "id": "https://catalogue.gaia-x.eu/credentials/ParticipantCredential/1655452007162",
    "issuer": "did:web:compliance.gaia-x.eu",
    "issuanceDate": "2022-06-17T07:46:47.162Z",
    "credentialSubject": {
      "id": "did:compliance.gaia-x.eu",
      "hash": "9ecf754ffdad0c6de238f60728a90511780b2f7dbe2f0ea015115515f3f389cd"
    },
    "proof": {
      "type": "JsonWebKey2020",
      "created": "2022-06-17T07:46:47.162Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..eQrh53-XXXXXXXXXXX",
      "verificationMethod": "did:web:vc.transmute.world#z6MksHh7qHWvybLg5QTPPdG2DgEjjduBDArV9EF9mRiRzMBN"
    }
  }
}
```

The output is synchronous when possible (all check automated)

| Component | Description | Batch | Owner | 
|---|---|---|---|
|DistributedAuthentication| Protocols / API / Services to use to authenticate users and a profile. To clarify ASAP | 1.1 | | 
|WebCatalog| Web pages to allow a user to create/update a Digital Trust Service | 1.3 |  |
|WebCompliance| Web pages to allow a user to add documents (ISO 27001 certification ...) to get a label | 1.5 | |
|APICompliance| API to add documents required to label a service| 1.5 | | 
|APICatalog| API to create/update services or search services | 1.3 | | 
|TrustAnchorRegistry| Registry of valid trust anchors | 1.5 | | 
| Portal| Gaia-x Portal of federation Portal | 1.5 | |

## Main dependencies

Distributed Authentication MUST provide:
* an authentication method (not ready :red_circle:)
* a way to get a user profile (not ready :red_circle:)

APICatalog MUST provide: 
* an API to get or update a service (any service) (not ready :red_circle:)

WebCatalog MUST provide:
* a way for a user to go from the catalog to the compliance service (not ready :red_circle:)
* define format for the Self Description of a Service

API Compliance MUST provide:
* an API to start a new compliance (not ready :red_circle:)
* an API to upload and store VC and pdf signed (not ready :red_circle:)
* an API to check a signature or an uploaded document (not ready :red_circle:)
* an api to validate / abord a compliance (not ready :red_circle:)

Digital Trust Service MUST provide: 
* an entry point to start a compliance (not ready :red_circle:)

TrustAnchorRegistry MUST provide : 
* an API to konw if a certificate is in the validation list (or not).  (not ready :red_circle:)

